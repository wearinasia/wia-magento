How To Install :

1. Clone WIA Master Git
2. Copy magento file to WIA Master Root
3. Create local DB and run magento installation from WIA Master folder
4. Start Develop

Main File Structure :

1. APP > Code > Local > "MODULENAME"
2. APP > etc > modules > "modulexmlfile.xml"
2. APP > Design > Frontend > wia > "ModulenameTemplateName"
2. SKIN > Frontend > wia > default > "moduleSkinName" 

Branch Structure :

1. Master -> Goes to production server
2. Develop -> Stagging / Testing in local server
3. Feature ( ex."feature/Modulname" ) -> Module Development
4. Hotfix -> Fixing bug or minor change to update on production server

How to create new Feature?
1. @gabeaventh create new feature-branch from develop
2. @gabeaventh assign team to the new feature-branch
3. @gabeaventh merge finish feature-branch to develop
4. @gabeaventh remove feature-branch after testing

How to fixing bug?
1. Create new hotfix-branch from master
2. Fixing bug
3. request @gabeaventh to merge into Develop
4. request @jamesroberto to merge into Master

Learn about Gitflow here: 
https://www.youtube.com/watch?v=ww4xpcvzLHc