<?php
 
class Bisnis_Exp_Block_Adminhtml_Host_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'bisnis_exp';
        $this->_controller = 'adminhtml_host_list';
        $this->_headerText = Mage::helper('bisnis_exp')->__('exp - host');
 
        parent::__construct();
        $this->_removeButton('add');
    }
}

?>