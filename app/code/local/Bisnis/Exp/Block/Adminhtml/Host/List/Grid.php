<?php
 
class Bisnis_Exp_Block_Adminhtml_Host_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('exp_grid');
        $this->setDefaultSort('exp_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        
       $collection = Mage::getResourceModel('customer/customer_collection') 
       ->addNameToSelect()
       ->addAttributeToSelect('email')
       ->addAttributeToSelect('created_at')
       ->addAttributeToSelect('group_id')
       ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
       ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
       ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
       ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
       ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left');

        

        $collection->addFilter('host_flag', 1, $type='and');
        //  var_dump($collection);
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('bisnis_exp');
        $currency = (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
 
        
        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('customer')->__('ID'),
            'width'     => '50px',
            'index'     => 'entity_id',
            'type'  => 'number',
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('customer')->__('Name'),
            'index'     => 'name'
        ));
        
        $this->addColumn('verified', array(
            'header' => $helper->__('#verified'),
            'index'  => 'verified'
        )); 
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('customer')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('customer')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));

        // $this->addExportType('*/*/exportInchooCsv', $helper->__('CSV'));
        // $this->addExportType('*/*/exportInchooExcel', $helper->__('Excel XML'));
 
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}

?>