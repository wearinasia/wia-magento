<?php
 
class Bisnis_Exp_Block_Adminhtml_Sales_Order extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'bisnis_exp';
        $this->_controller = 'adminhtml_sales_order';
        $this->_headerText = Mage::helper('bisnis_exp')->__('exp - account');
 
        parent::__construct();
        $this->_removeButton('add');
    }
}

?>