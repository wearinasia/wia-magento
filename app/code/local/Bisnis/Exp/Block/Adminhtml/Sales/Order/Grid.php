<?php
 
class Bisnis_Exp_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('exp_grid');
        $this->setDefaultSort('exp_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('exp/account')->getCollection();
        
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('bisnis_exp');
        $currency = (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
 
        $this->addColumn('acc_id', array(
            'header' => $helper->__('#acc_id'),
            'index'  => 'acc_id'
        ));
 
        $this->addColumn('acc_name', array(
            'header' => $helper->__('Name'),
            'index'  => 'acc_name'
        ));

        $this->addColumn('acc_number', array(
            'header' => $helper->__('acc_number'),
            'index' => 'acc_number'
        ));

        $this->addColumn('bank_name', array(
            'header' => $helper->__('bank_name'),
            'index' => 'bank_name'
        ));
 
        $this->addColumn('customer_id', array(
            'header' => $helper->__('customer_id'),
            'index' => 'customer_id'
        ));
//        $this->addExportType('*/*/exportInchooCsv', $helper->__('CSV'));
//        $this->addExportType('*/*/exportInchooExcel', $helper->__('Excel XML'));
 
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}

?>