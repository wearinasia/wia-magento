<?php
 
class Bisnis_Exp_Block_Adminhtml_Schedule_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('exp_grid');
        $this->setDefaultSort('exp_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('exp/schedule')->getCollection();
        // $hai = Mage::getResourceModel('customer/customer_collection') 
        // ->addAttributeToSelect('firstname');

//        var_dump($hai);

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('bisnis_exp');
        $currency = (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
 
        $this->addColumn('schedule_id', array(
            'header' => $helper->__('#schedule_id'),
            'index'  => 'schedule_id'
        ));
        
        $this->addColumn('customer_id', array(
            'header' => $helper->__('#customer_id'),
            'index'  => '1'
        ));

        $this->addColumn('product_id', array(
            'header' => $helper->__('product_id'),
            'index'  => 'product_id'
        ));

        $this->addColumn('date', array(
            'header' => $helper->__('date'),
            'index' => 'date'
        ));

       
 
        // $this->addExportType('*/*/exportInchooCsv', $helper->__('CSV'));
        // $this->addExportType('*/*/exportInchooExcel', $helper->__('Excel XML'));
 
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}

?>