<?php
 
class Bisnis_Exp_Block_Adminhtml_Withdrawal_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'bisnis_exp';
        $this->_controller = 'adminhtml_withdrawal_list';
        $this->_headerText = Mage::helper('bisnis_exp')->__('exp - withdrawal');
 
        parent::__construct();
        $this->_removeButton('add');
    }

}

?>