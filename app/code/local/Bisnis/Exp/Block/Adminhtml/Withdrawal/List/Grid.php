<?php
 
class Bisnis_Exp_Block_Adminhtml_Withdrawal_List_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('exp_grid');
        $this->setDefaultSort('exp_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
 
    protected function _prepareCollection()
    {
        // $collection = Mage::getModel('exp/withdrawal')->getCollection();
        // $collection->getSelect()->join(array('account'=>'accounts'),
        // 'account.acc_id = main_table.acc_id', array('account.acc_name'));
        $collection = Mage::getModel('sales/order')->getCOllection()->addFieldToFilter('status', 'complete');

       // $joinnama = Mage::getModel('customer/customer')->getCollection();
        // ->addAttributeToSelect('First_name');
        
        //var_dump($joinnama);

 //       var_dump($joinnama->getSelect());
        //  $namahost = Mage::getResourceModel('customer/customer_entity')
        //     ->join(array('a'=>'exp/account'))
        // ->addExpressionFieldToSelect(
        //    'acc_id',

        //var_dump($collection);

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }
 
    protected function _prepareColumns()
    {
        $helper = Mage::helper('bisnis_exp');
        $currency = (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
 
        $this->addColumn('customer_id', array(
            'header' => $helper->__('customer_id'),
            'index'  => 'customer_id'
        ));

        
        $this->addColumn('increment_id', array(
            'header' => $helper->__('increment_id'),
            'index'  => 'increment_id'
        ));
       
    
       
        // $this->addExportType('*/*/exportInchooCsv', $helper->__('CSV'));
        // $this->addExportType('*/*/exportInchooExcel', $helper->__('Excel XML'));
            
        return parent::_prepareColumns();
    }
 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
}

?>