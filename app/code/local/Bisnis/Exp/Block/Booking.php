<?php

class Bisnis_Exp_Block_Booking extends Mage_Core_Block_Template {
    public function _prepareLayout(){
        return parent::_prepareLayout();

    }

    public function getSession()
    {
        $session = Mage::getSingleton('core/session');
        $SID = $session->getEncryptedSessionId();
        return $SID;
    }
    
    public function getProduct()
	{
    	$product = Mage::registry('current_product');
    	return $product;
    }

   
    public function getAvailability()
	{
    	
    	return '1';
    }


    public function getOrder()
	{
    	
    	    $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customerData->getId();
            
            $collection = Mage::getResourceModel('catalog/product_collection')
            //->addAttributeToSelect(array('sku', 'name', 'description'))
            ->addFieldToFilter('customer_id', $id);
            foreach ($collection as $item) {
                $idproduk[] = $item->getId();
            }
            //$idproduk="4046";

            $ordermodel = Mage::getResourceModel('sales/order_item_collection')
            ->addAttributeToFilter('product_id', array('in', $idproduk))
            ->load();

            
            
            $order_ids = $ordermodel->getColumnValues('order_id');
            
            return $order_ids;
    }
    
    public function getOrderDetails($order_id){
        $order = Mage::getModel('sales/order')->load($order_id);
        return $order;
    }

    public function getOrderDetail()
	{
        
        $order_id = $this->getRequest()->getParam('id');
        $session_key = $this->getRequest()->getParam('key');
        
        $order = Mage::getModel('sales/order')->load($order_id);
        return $order;
    }

    public function getOrderById(){
        //return $this->getData('product_id');
        $order_id = $this->getData('order_id');
        $order = Mage::getModel('sales/order')->load($order_id);
        return $order;
    }

    

    public function getProductDetail()
	{
        $order = $this->getOrderDetail();
       
        $items = $order->getAllVisibleItems();

        $productIds = array();
        foreach($items as $i) {
            $productId = $i->getProductId();
        }

        $product = Mage::getModel('catalog/product')->load($productId);

        return $product;
        
       
    }
    


    
    

}

?>