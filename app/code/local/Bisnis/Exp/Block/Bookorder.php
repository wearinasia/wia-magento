<?php

class Bisnis_Exp_Block_Bookorder extends Mage_Core_Block_Template {
    public function _prepareLayout(){
        return parent::_prepareLayout();

    }

    ////untuk narik info customer
    public function getCustomer(){
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $id = $customerData->getId();
        $obj = $this->_cust = Mage::getModel('customer/customer')->load($id);
        return $obj;
    }

    //dipake di myexperience page myexperience.phtml
    public function selectProduct(){
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $id = $customerData->getId();
        //$gabung [] = array();
        $collection = Mage::getResourceModel('catalog/product_collection')
        ->addAttributeToSelect('*')
        ->addFieldToFilter('customer_id', $id);
        //buat main didepan foreachnya.. dikoment next
            // foreach ($collection as $item) {
            //     $sku = $item->getSku();
            //     $name= $item->getName();
            //     $description = $item->getDescription(); 
            //     $gabung[] = array($sku, $name, $description);
            // }
        return $collection;

    }



    // dipake di booking list page bookorder.phtml == order dari customer
    public function selectOrder(){
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customerData->getId();
            
            $collection = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(array('sku', 'name', 'description'))
            ->addFieldToFilter('customer_id', $id);
            foreach ($collection as $item) {
                $idproduk[] = $item->getId();
            }
             
            $ordermodel = Mage::getResourceModel('sales/order_item_collection')
            ->addAttributeToFilter('product_id', array('in', $idproduk))
            ->load();
            $order_ids = $ordermodel->getColumnValues('order_id');
            //setcollection($order_ids);
            //dipisah biar didepan rapih
            // foreach($order_ids as $order_id){
            //     $order = Mage::getModel('sales/order')->load($order_id);
            //     $idpembeli =  $order->getId(); // order id
            //     $nama =  $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname();
            //     //echo $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname();
            //     //echo $order->getBaseGrandTotal(); // BaseGrandTotal
            //     //echo $order->getGrandTotal(); // GrandTotal
            //     $status = $order->getStatus(); // Status
            //     $gabung[] = array($idpembeli, $nama, $status);
            // } 
            return $order_ids;
    }
    
    // dipake di inbox order untuk nunjukin order as a customer
    public function selectorderviewcust(){
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $id = $customerData->getId();

        $ordercustomer = Mage::getModel('sales/order')->getCollection()
            ->addAttributeToFilter('customer_id', $id)
            ->addFieldToFilter('state', 'processing')
            ->addFieldToSelect('*');   
        
        foreach($ordercustomer as $ox){
            $inc =  $ox->getIncrementId();
            $orderan = Mage::getModel('sales/order')->loadByIncrementId($ox->getIncrementId());
            $items = $orderan->getAllVisibleItems();
            
            foreach($items as $itm){
                $produkid = $itm->getProductId();
                $produkmodel = Mage::getModel('catalog/product')->load($produkid);
                $modelhost = Mage::getModel('customer/customer')->load($produkmodel['customer_id']);
                $nama = $modelhost['firstname'];
            }
            $status = $ox->getStatus();
            
            $parameter[] = array(
                'increment_id' => $inc, 
                'nama'  => $nama, 
                'status' => $status
            );

        }
        return $parameter;
    }


    // //untuk di pake di payoutmethod 
    // public function selectpayoutmethod(){
    //         $customerData = Mage::getSingleton('customer/session')->getCustomer();
    //         $id = $customerData->getId();
    //         $payoutmodel = Mage::getModel('exp/account')->getCollection();
    //         $payoutmodel->addFieldToFilter('customer_id', $id);
    //         //->addAttributeToFilter('customer_id', $id);
    //         return ($payoutmodel) ;
    // }
  
    //dipake di payout ketika complete dan belum dibayar
    public function cariorder($ord){
        $order = Mage::getModel('sales/order')->load($ord);      
        return $order;
    }

    //buat formbookorder.phtml
    public function cariorderbydate($idprdk){
        
        // buat attribute value dan label !!!!!!
        // $attributevalue = Mage::getResourceModel('catalog/product')->getAttributeRawValue(13, 'januari_coba', 4);
        // //var_dump($attributevalue);
        // $product = Mage::getModel('catalog/product')
        // ->setStoreId(4)
        // ->setData('januari_coba',$attributevalue); 
        // $option_label = $product->getAttributeText('januari_coba');
        // //var_dump($option_label);

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $id = $customerData->getId();
        $idproduks [] = array();
        $collection = Mage::getResourceModel('catalog/product_collection')
        ->addAttributeToSelect(array('sku', 'name', 'description'))
        ->addFieldToFilter('customer_id', $id);

        foreach ($collection as $item) {
            $idproduk[] = $item->getId();
        }
        
        ///masih di hardcode bookdatenya.
        $ordermodel = Mage::getResourceModel('sales/order_item_collection')
        ->addAttributeToFilter('product_id', array('in', $idprdk))
        ->load();
        $order_ids = $ordermodel->getColumnValues('order_id');        
        foreach($order_ids as $order_id){
            $order = Mage::getModel('sales/order')->load($order_id);
            $status = $order->getBookdate(); // bookdate ketika order 
            if ($order->getStatus()=='complete' || $order->getStatus()=='processing'){
                $gabung[] = ($status);
            }
            
        }
        $countedArray = array_count_values($gabung);
        $total = count($countedArray);
        return ($countedArray);
        //return $order_ids;

    
    }

    //untuk liat slot sisa nya berapa 
    public function blokqty($idprdk){
        $product = Mage::getModel("catalog/product")->load($idprdk);
        $tetapan = $product->getTetapanqty();
        
        return $tetapan;
    }
    //buat nampilin chat sebelumnya 
    // public function riwayatchat($ord){
    //     $orderId = $ord;
    //     $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
    //     $allcomment = $order->getAllStatusHistory();
    //     foreach ($allcomment as $comment) {
    //         $body = $comment->getData('comment');
    //         echo $body. "</br>";
    //     }
    // }
    //dipakai di bookingdetail
    public function selectbooking(){
        $params = $this->getRequest()->getParams();
        $getOrder = Mage::getModel('sales/order')->loadByIncrementId($params['incrementid']);
        return $getOrder;

    }
    

}



?>