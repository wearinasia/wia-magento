<?php

class Bisnis_Exp_Block_Inboxorder extends Mage_Core_Block_Template {

    public function getSession()
        {
            $session = Mage::getSingleton('core/session');
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }

        public function validateSession()
        {
            $session = $this->getSession();
            $session_key = $this->getRequest()->getParam('key');  
            if ($session_key !== $session):
                $this->_redirectReferer(); 
                Mage::getSingleton('core/session')->addError('Your session is expired');
                return;
            endif;
        }
    
        public function getCustomer()
        {
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            
            if ($customer->getId()):
                return $customer;
            endif;
            return false;
        }

    
    
  
    public function selectOrder(){
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $id = $customerData->getId();
        $gabung [] = array();
        $idproduks [] = array();
        $collection = Mage::getResourceModel('catalog/product_collection')
        ->addAttributeToSelect(array('sku', 'name', 'description'))
        ->addFieldToFilter('customer_id', $id);
        foreach ($collection as $item) {
            $idproduk[] = $item->getId();
        }
         
        $ordermodel = Mage::getResourceModel('sales/order_item_collection')
        ->addAttributeToFilter('product_id', array('in', $idproduk))
        ->load();
        $order_ids = $ordermodel->getColumnValues('order_id');
        //setcollection($order_ids);
        //dipisah biar didepan rapih
        // foreach($order_ids as $order_id){
        //     $order = Mage::getModel('sales/order')->load($order_id);
        //     $idpembeli =  $order->getId(); // order id
        //     $nama =  $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname();
        //     //echo $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname();
        //     //echo $order->getBaseGrandTotal(); // BaseGrandTotal
        //     //echo $order->getGrandTotal(); // GrandTotal
        //     $status = $order->getStatus(); // Status
        //     $gabung[] = array($idpembeli, $nama, $status);
        // } 
        return $order_ids;
    }
    public function selectorderviewcust(){
        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $id = $customerData->getId();

        $ordercustomer = Mage::getModel('sales/order')->getCollection()
            ->addAttributeToFilter('customer_id', $id)
            ->addFieldToFilter('state', 'processing')
            ->addFieldToSelect('*');   
        
        foreach($ordercustomer as $ox){
            $inc =  $ox->getIncrementId();
            $orderan = Mage::getModel('sales/order')->loadByIncrementId($ox->getIncrementId());
            $items = $orderan->getAllVisibleItems();
            
            foreach($items as $itm){
                $produkid = $itm->getProductId();
                $produkmodel = Mage::getModel('catalog/product')->load($produkid);
                $modelhost = Mage::getModel('customer/customer')->load($produkmodel['customer_id']);
                $nama = $modelhost['firstname'];
            }
            $status = $ox->getStatus();
            
            $parameter[] = array(
                'increment_id' => $inc, 
                'nama'  => $nama, 
                'status' => $status
            );

        }
        return $parameter;
    }

   

    public function getCustomerName($customerId){
        $customer = Mage::getModel('customer/customer')->load($customerId);
        return $customer->getName();
    }
    public function getOrder(){
        
        $params = $this->getRequest()->getParam('id');
        $order = Mage::getModel('sales/order')->load($params);

        return $order;

       
    }

}

?>