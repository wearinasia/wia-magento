<?php

class Bisnis_Exp_Block_Product extends Mage_Core_Block_Template {
    public function _prepareLayout(){
        return parent::_prepareLayout();

    }

    public function getCustomer()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        
        if ($customer->getId()):
            return $customer;
        endif;
        return false;
    }

    public function getCalendar($month,$year) {

        // Create array containing abbreviations of days of week.
        $daysOfWeek = array('S','M','T','W','T','F','S');
   
        // What is the first day of the month in question?
        $firstDayOfMonth = mktime(0,0,0,$month,1,$year);
   
        // How many days does this month contain?
        $numberDays = date('t',$firstDayOfMonth);
   
        // Retrieve some information about the first day of the
        // month in question.
        $dateComponents = getdate($firstDayOfMonth);
   
        // What is the name of the month in question?
        $monthName = $dateComponents['month'];
   
        // What is the index value (0-6) of the first day of the
        // month in question.
        $dayOfWeek = $dateComponents['wday'];
   
        // Create the table tag opener and day headers
   
        $calendar = "<table id='calendar' class='calendar'>";
        $calendar .= "<caption>$monthName $year</caption>";
        $calendar .= "<tr>";
   
        // Create the calendar headers
   
        foreach($daysOfWeek as $day) {
             $calendar .= "<th class='header'>$day</th>";
        } 
   
        // Create the rest of the calendar
   
        // Initiate the day counter, starting with the 1st.
   
        $currentDay = 1;
   
        $calendar .= "</tr><tr>";
   
        // The variable $dayOfWeek is used to
        // ensure that the calendar
        // display consists of exactly 7 columns.
   
        if ($dayOfWeek > 0) { 
             $calendar .= "<td colspan='$dayOfWeek'></td>"; 
        }
        
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
     
        while ($currentDay <= $numberDays) {
   
             // Seventh column (Saturday) reached. Start a new row.
   
             if ($dayOfWeek == 7) {
   
                  $dayOfWeek = 0;
                  $calendar .= "</tr><tr>";
   
             }
             
             $currentDayRel = str_pad($currentDay, 2, "0", STR_PAD_LEFT);
             
             $date = "$year-$month-$currentDayRel";
   
             $calendar .= "<td id='$date' data-recnum='$date' class='day' rel='$date'>$currentDay</td>";
   
             // Increment counters
    
             $currentDay++;
             $dayOfWeek++;
   
        }
        
        
   
        // Complete the row of the last week in month, if necessary
   
        if ($dayOfWeek != 7) { 
        
             $remainingDays = 7 - $dayOfWeek;
             $calendar .= "<td colspan='$remainingDays'>&nbsp;</td>"; 
   
        }
        
        $calendar .= "</tr>";
   
        $calendar .= "</table>";
   
        return $calendar;
   
   }

    

    public function getSession()
    {
        $session = Mage::getSingleton('core/session');
        $SID = $session->getEncryptedSessionId();
        return $SID;
    }
    
    public function getProductCollection(){
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $customer_id = $customer->getId();
        $_productCollection = Mage::getModel('catalog/product')
                    ->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('customer_id', $customer_id)
                    ->addAttributeToSort('created_at', 'DESC');
                    //->setPageSize(8	);
        return $_productCollection;
    }
    
    public function getProduct()
	{
    	$id = $this->getRequest()->getParam('id');
    	return Mage::getModel('catalog/product')->load($id);
    }

    
    
    
    
}

?>