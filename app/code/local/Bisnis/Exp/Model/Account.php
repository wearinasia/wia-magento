<?php 

class Bisnis_Exp_Model_Account extends Mage_Core_Model_Abstract {
    protected function _construct()
    {
        $this->_init('exp/account');
    }    
}

class Bisnis_Exp_Model_Host extends Mage_Core_Model_Abstract {
    protected function _construct()
    {
        $this->_init('exp/host');
    }    
}


?>