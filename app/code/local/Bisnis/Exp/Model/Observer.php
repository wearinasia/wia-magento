<?php

class Bisnis_Exp_Model_Observer

{
	public function sales_order_save_after($observer)
	{
		
        $order = $observer->getOrder();
        $order_id = $order->getIncrementId();
        $shipping = $order->getShippingMethod();
        $status = $order->getStatus();
        
        //notif booking confirmation to customer & host
        if($shipping == 'flatrate_flatrate' && $status == "processing"){
            $this->send('Booking Confirmation', 38 ,$order_id);
            $this->send('New Booking Order', 37 ,$order_id);
        }

        //notif booking checkout to customer & notify host to request payout
        if($shipping == 'flatrate_flatrate' && $status == "processing_checkout"){
            $this->send('Thankyou, you have complete your experience', 36 ,$order_id);
            $this->send('Your payout now Available!', 35 ,$order_id);
        }

        //notify admin a new payout request & notify host, payout request receive
        if($shipping == 'flatrate_flatrate' && $status == "complete_payout_request"){
            $this->send('New Payout Request', 35 ,$order_id);
        }

        //notify host, payout paid
        if($shipping == 'flatrate_flatrate' && $status == "complete"){
            $this->send('Your payout request have been paid', 34 ,$order_id);
        }

       	//return $this;
    }
    
    public function send($title, $templateId, $order_id)
        {
            
            // $senderName = Mage::getStoreConfig('trans_email/ident_support/name');  //Get Sender Name from Store Email Addresses
            // $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');  //Get Sender Email Id from Store Email Addresses
            // $sender = array('name' => $senderName,
            //             'email' => $senderEmail);

            // // Set recepient information
            // $recepientEmail = 'jamesrobertoangrianto@gmail.com';
            // $recepientName = 'james';      

            // // Get Store ID     
            // $storeId = Mage::app()->getStore()->getId();

            // // Set variables that can be used in email template
            // $vars = array(
            //     'emailTitle' => $title,
            //     'order' => $order_id,
            // ); 
        
            // // Send Transactional Email
            // Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $vars, $storeId);

            //$supplier_id = $item['id'];
				
			//$supplierDetails = Mage::getModel('inventorysystem/managesupplier')->load($supplier_id);
            
            //$supplierEmail = $supplierDetails->getEmail();
			
			// $storeObj = Mage::app()->getStore()->getId();
            // $customerEmailId = 'jamesrobertoangrianto@gmail.com';
            // $customerFName = 'james';
            
            // //load the custom template to the email  
            // $emailTemplate = Mage::getModel('core/email_template')->load(38);
            
            
            
            // $emailTemplateVariables = array();
            
            // $emailTemplateVariables['order'] = $order;
           	// $emailTemplateVariables['store'] = $storeObj;
            // $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_support/name'));
            // $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_support/email'));
            

            
            // $emailTemplate->setType('html');
            // $emailTemplate->setTemplateSubject('Pesanan baru dari Wearinasia.com');
            // $emailTemplate->send($customerEmailId, $customerFName , $emailTemplateVariables);
				
            
        }
	
	
	

}

?>