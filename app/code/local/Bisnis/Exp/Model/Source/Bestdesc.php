<?php


class Bisnis_Exp_Model_Source_Bestdesc extends Mage_Eav_Model_Entity_Attribute_Source_Abstract {

    public function getAllOptions(){

        if (is_null($this->_options)) {
            $this->_options = array(

                array(
                    "label" => Mage::helper("eav")->__("a unique activity that guests can't do on their own"),
                    "value" =>  1
                ),

                array(
                    "label" => Mage::helper("eav")->__("a unique angle on an activity that guests can do on their own"),
                    "value" =>  2
                ),
                array(
                    "label" => Mage::helper("eav")->__("a convenient way for guests to see tourist landmarks, neighborhoods, or other travel destinations"),
                    "value" =>  3
                )
            );
        }
        return $this->_options;


    }
    
    public function getOptionArray(){
        $_options = array();
        foreach ($this->getAllOptions() as $option) {
            $_options[$option["value"]] = $option["label"];
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value) {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }




}




?>