<?php 

    class Bisnis_Exp_AccountController extends Mage_Core_Controller_Front_Action {
        
        public function getSession()
        {
            $session = Mage::getSingleton('core/session');
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }

        public function validateSession()
        {
            $session = $this->getSession();
            $session_key = $this->getRequest()->getParam('key');  
            if ($session_key !== $session):
                $this->_redirect('');
                Mage::getSingleton('core/session')->addError('Your session is expired');
                return;
            endif;
        }

        public function EditAction()
        {
           // $this->validateSession();
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->renderLayout();
        }

        public function ViewAction()
        {
           
            $this->loadLayout();
            $this->renderLayout();
        }

        public function EditPostAction()
        {
            $params = $this->getRequest()->getParams(); 
            $customer = Mage::getModel('customer/customer')->load($params['customer_id']);
            
                $customer->setCustomerBio($params['customer_bio'])->save();
                //$customer->setCustomerPhotoUrl($params['customer_photo_url'])->save();
                $customer->setCustomerJob($params['customer_job'])->save();
            
           // echo $customer->getId();

           $this->_redirect('customer/account');
        }


        public function uploadProfilePhotoAction(){
                
            $params = $this->getRequest()->getParams(); 
            
            $file_dir  = Mage::getBaseDir('media') . DS . 'host' . DS ; // where we save images  
            echo $customer_id = $params['customer_id'];

            if ($data = $this->getRequest()->getParams()) {
                $type = 'file';
                //echo $_FILES['file']['name'];

                foreach ($_FILES['file']['name'] as $key => $value){
                    if (isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
                    
                    try {
                        //$uploader = new Varien_File_Uploader($_FILES['file']['name'][$key]);
                        $uploader = new Varien_File_Uploader(
                            array(
                        'name' => $_FILES['file']['name'][$key],
                        'type' => $_FILES['file']['type'][$key],
                        'tmp_name' => $_FILES['file']['tmp_name'][$key],
                        'error' => $_FILES['file']['error'][$key],
                        'size' => $_FILES['file']['size'][$key]
                            )
                    );
                        


                        $uploader
                            ->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                        $uploader->setAllowRenameFiles(true);
                        $uploader->setFilesDispersion(true);
                        
                        $path = Mage::getBaseDir('media') . DS . 'host';
                        if (!is_dir($path)) {
                            mkdir($path, 0777, true);
                        }
                        $uploader->save($path, $_FILES['file']['name'][$key]);
                        
                        //$uploaded_path = $path;
                        $uploaded_name = $uploader->getUploadedFileName();
                        $customer = Mage::getModel('customer/customer')->load($customer_id);
                        $customer->setCustomerPhotoUrl($uploaded_name)->save();
       


                    } catch (Exception $e) {
                        Mage::log($e->getMessage(), null, $this->_logFile);
                    }
                    


                }

                

                }
                
            }

            //echo $uploaded_name;
            //$product->save();
            
           
            
            $this->_redirect('customer/account');

    }

       
      





    }

?>