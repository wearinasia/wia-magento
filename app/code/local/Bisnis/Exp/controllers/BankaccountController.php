<?php 

    class Bisnis_Exp_BankaccountController extends Mage_Core_Controller_Front_Action {
        
        public function IndexAction(){
            if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                echo('mohon login terlebih dahulu');
            }else{
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('bank account');
                $this->renderLayout();
            }
        }

        public function addpayoutmethodAction(){
            $params = $this->getRequest()->getParams();
            $id = $params['idsession'];

            $payoutmodel = Mage::getModel('exp/account')->getCollection();
            $payoutmodel->addFieldToSelect('customer_id', $id);
            $kumpulanid [] = array ();

            foreach($payoutmodel as $data) {
                $kumpulanid[] = $data['customer_id'];
            }

            if (in_array($id, $kumpulanid)){
                $this->_redirect('*/*/');
                Mage::getSingleton("core/session")->addSuccess('hanya bisa mendaftarkan 1 akun'); 
                
                // echo "gabisa daftar lagi" ;
                // $url = Mage::getUrl("exp/payoutmethod/index");
                // Mage::app()->getResponse()
                //     ->setRedirect($url, 301)
                //     ->sendResponse();

            }else{
                $modelaccount = Mage::getModel('exp/account');
                $namaakun = $params['acc_name'];
                $nomorakun = $params['acc_number'];
                $namabank = $params['bank_name'];
                $modelaccount->setAccName($namaakun);
                $modelaccount->setAccNumber($nomorakun);
                $modelaccount->setBankName($namabank);
                $modelaccount->setCustomerId($id);
                $modelaccount->save();
                echo "berhasil" ;

                $url = Mage::getUrl("exp/bankaccount/index");
                Mage::app()->getResponse()
                    ->setRedirect($url, 301)
                    ->sendResponse();
                

            }
        }

        public function editPostAction(){
            
            if (!$this->_validateFormKey()) {
                $this->_redirect('*/*');
                return;
            }

            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customer_id = $customerData->getId();

            $params = $this->getRequest()->getParams();
            $namaakun = $params['acc_name'];
            $nomorakun = $params['acc_number'];
            $namabank = $params['bank_name'];
            $id = $params['idsession'];
            $acc_id = $params['acc_id'];

            $modelaccount = Mage::getModel('exp/account')->load($acc_id);
            $ids = $modelaccount->getCustomerId();
           
            $modelacc = Mage::getModel('exp/account')->getCollection();
            
            if ($ids == $customer_id ){
                foreach ($modelacc as $data) {
                    if($data['customer_id']== $id){
                        $data->setAccName($namaakun);
                        $data->setAccNumber($nomorakun);
                        $data->setBankName($namabank);
                        $data->save();
                    }
                }
                echo "bisa update";
                $url = Mage::getUrl("exp/bankaccount/index");
                Mage::app()->getResponse()
                    ->setRedirect($url, 301)
                    ->sendResponse();
            }else{
                $this->_redirect('*/*/');
                Mage::getSingleton("core/session")->addSuccess('tidak ada akses untuk mengedit'); 
                
            }
  
            

        }

        public function addAction(){
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('form-bank account');
            $this->renderLayout();
        }

        public function editAction(){
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('form-bank account');
            $this->renderLayout();
        }


        






    }

?>