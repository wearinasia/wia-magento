<?php 

    class Bisnis_Exp_BookingController extends Mage_Core_Controller_Front_Action {
        
        public function getSession()
        {
            $session = Mage::getSingleton('core/session');
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }

        public function validateSession()
        {
            $session = $this->getSession();
            $session_key = $this->getRequest()->getParam('key');  
            if ($session_key !== $session):
                $this->_redirect('');
                Mage::getSingleton('core/session')->addError('Your session is expired');
                return;
            endif;
        }



      
        public function indexAction(){
            $params = $this->getRequest()->getParam('guest');
            echo Mage::helper('core')->jsonEncode($params);

        }
        
        
        
       
        // render page
        public function addAction()
        {
            $this->validateSession();
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->renderLayout();
        }


        // render view order page
        public function listAction()
        {
            
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->renderLayout();
        }
        // render list order page
        public function viewAction()
        {
            $this->validateSession();

            

            $this->loadLayout();
            $this->renderLayout();
        }

        public function reviewAction()
        {
            $this->validateSession();
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->renderLayout();
        }

        // dipake di bookorder
        public function addtocartAction(){
            //$this->validateSession();
            $prms = $this->getRequest()->getParams();
            $bookdate=$prms['bookdate'];
            $productid=$prms['productid'];
            $qty = $prms['qty'];

            $params = array(
                'product' => $productid,
                'qty' => $qty,
                'bookdate' => $bookdate,
            );

             
            $session = Mage::getSingleton('customer/session');
            $session->setBookSession($params);
            
           

            $url = Mage::getUrl('exp/booking/add', array('key'=>$this->getSession()));
            Mage::app()->getResponse()
                ->setRedirect($url, 301)
                ->sendResponse();
            //var_dump($params);


        }

        private function _checkAvailability(){
            //receive parameter from post
            $params = $this->getRequest()->getParams();
            $idprdk = $params['product_id'];
            $tgl = $params['bookdate'];

            //load order model by product id
            $ordermodel = Mage::getResourceModel('sales/order_item_collection')
            ->addAttributeToFilter('product_id', array('in', $idprdk))
            ->load();
            $order_ids = $ordermodel->getColumnValues('order_id');  
            

            //load product model by product id
            $product = Mage::getModel("catalog/product")->load($idprdk);
            $max_availability = $product->getTetapanqty();
           

            //check total order by product id
            foreach($order_ids as $order_id){
                $order = Mage::getModel('sales/order')->load($order_id);
                // count as order if status is:

                if ($order->getState() == 'new' || $order->getState() == 'processing' || $order->getState() == 'complete'){
                    

                    //echo $order->getId();
                    if($order->getBookdate() == $tgl ){
                        
                        $qty[] = $order->getTotalQtyOrdered();
                        
                        
                    }
                    
                    
                }
            }
            
            $ordered_qty = array_sum($qty);
            $availability = $max_availability - $ordered_qty;
            return $availability;

            if( $availability >= 0){
                return $availability;
            }
            else{
                return 0;
            }
        }
        
        public function getAvailabilityAction(){
            
            $availability = $this->_checkAvailability();
            
            echo $availability;
           
            
           
        }

        public function updatePostAction(){
            
            $product_id = $this->getRequest()->getParam('id');
            $order = Mage::getModel('sales/order')->load($product_id);
            
            $status = $order->getStatus();
            //var_dump($status);
            
            
            if($status == "processing"){
                
                $order->setData('state', "processing");
                $order->setStatus("processing_checkin");
                $order->addStatusHistoryComment('Customer Checkin');
              
               
                
            }

           
            
            if($status=="processing_checkin"){
                $order->setData('state', "processing");
                $order->setStatus("processing_checkout");
                $order->addStatusHistoryComment('Customer Checkout');
                
               
                
            }
            if($status=="processing_checkout"){
                $order->setData('state', "complete");
                $order->setStatus("complete_payout_request");
                $order->addStatusHistoryComment('Host request for payout');
                
               
                
            }

            try {
                $order->save();
                Mage::getSingleton("core/session")->addSuccess("Update success"); 
                $this->_redirectReferer();
            } catch (Exception $e) {
                Mage::getSingleton("core/session")->addError("Update fail"); 
                $this->_redirectReferer(); 
            }

        }
    
        public function saveGuestPostAction(){
            $params = $this->getRequest()->getParams();
            $name = $params['name'];
            $email = $params['email'];
            $phone = $params['phone'];
            $note = $params['note'];

            $params = array(
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'note' => $note,
            );

            $session = Mage::getSingleton('customer/session');
            $session->setGuestSession($params);
            $this->_redirect('exp/booking/review',array('key'=>$this->getSession()));

        }

  

        public function addPostAction(){

            

            $store = Mage::app()->getStore();
            $website = Mage::app()->getWebsite();

            //Create sales quote object
            $quote = Mage::getModel('sales/quote')->setStoreId($store->getStoreId());

            $params = $this->getRequest()->getParams();

            $customer = Mage::getSingleton('customer/session')->getCustomer();
            
            

            // $customerEmail = $params['email'];
            // $customerFirstname = $params['name'];
            // $customerLastname =  $params['name'];

            //$customerEmail = $customer->getEmail();
            $customerFirstname = $customer->getFirstname();
            $customerLastname =  $customer->getLastname();

            $customerPhone =  $params['phone'];
            $customerNote =  $params['note'];
            $productQty =  $params['qty'];
            $productid =  $params['product_id'];
            $bookdate =  $params['bookdate'];
            $information = $params['guest'];
            
          
            
            $guest_info = json_encode($information, true);
            $billingAddress = array(
                        //'customer_address_id' => '',
                        'prefix' => '',
                        'firstname' => $customerFirstname,
                        'middlename' => '',
                        'lastname' => $customerLastname,
                        'suffix' => '',
                        'company' => '', 
                        'street' => array(
                            '0' => 'No Address', // compulsory
                            
                        ),
                        'city' => 'No City',
                        'country_id' => 'ID', // two letters country code
                        'region' => 'Indonesia', // can be empty '' if no region
                        'region_id' => 'ID', // can be empty '' if no region_id
                        'postcode' => '0000',
                        'telephone' => $customerPhone,
                        'fax' => '',
                        //'save_in_address_book' => 1
                    );

            $shippingMethod = 'flatrate_flatrate';
            
            $paymentMethod = 'snap';
            
            
            // Initialize sales quote object
            $quote = Mage::getModel('sales/quote')->setStoreId($store->getId());
            
            // Set currency for the quote
            $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
            
            //Check whether the customer already registered or not
            //$customer = Mage::getModel('customer/customer')->setWebsiteId($website->getId())->loadByEmail($customerEmail);

            // if (!$customer->getId()) {

            //     //Create the new customer account if not registered
            //     $customer = Mage::getModel('customer/customer'); 
            //     $customer->setWebsiteId($website->getId())
            //             ->setStore($store)
            //             ->setFirstname($customerFirstname)
            //             ->setLastname($customerLastname)
            //             ->setEmail($customerEmail);

            //     try {
            //         //$password = $customer->generatePassword(); 
            //         $customer->setPassword($customerPhone);

            //         //Set the customer as confirmed
            //         $customer->setForceConfirmed(true);
            //         $customer->save();

            //         $customer->setConfirmation(true);
            //         $customer->save();

        
            //         //Send new account email to customer
            //         $storeId = $customer->getSendemailStoreId();
            //         $customer->sendNewAccountEmail();

            //         //Set password remainder email if the password is auto generated by magento
            //         //$customer->sendPasswordReminderEmail();

            //     } catch (Exception $e) {
            //         Mage::logException($e);
            //     } 
            // }
           
            
            
            
            // Assign customer to quote
            $quote->assignCustomer($customer);
            

            //Add products to quote
            $product = Mage::getModel('catalog/product')->load($productid);
            $quote->addProduct($product, $productQty);
            
            // Add billing address to quote
            $billingAddressData = $quote->getBillingAddress()->addData($billingAddress);
            
            // Add shipping address to quote
            $shippingAddressData = $quote->getShippingAddress()->addData($billingAddress);
            
            // Collect shipping rates on quote shipping address data
            $shippingAddressData->setCollectShippingRates(true)
                                ->collectShippingRates();
            
            // Set shipping and payment method on quote shipping address data
            $shippingAddressData->setShippingMethod($shippingMethod)
                                ->setPaymentMethod($paymentMethod);
            
            // Set payment method for the quote
            $quote->getPayment()->importData(array('method' => $paymentMethod));
            
            try {
                // Collect totals of the quote
                $quote->collectTotals();
                $quote->setBookdate($bookdate);
                $quote->setInfoguest($guest_info);
                // Save quote
                $quote->save();
                
                // Create Order From Quote
                $service = Mage::getModel('sales/service_quote', $quote);
                $service->submitAll();
                $order = $service->getOrder();

                $this->sendNewOrderMail($customer, $order, $productid);
                

                $incrementId = $service->getOrder()->getRealOrderId();
                
                Mage::getSingleton('checkout/session')
                
                    ->setRealOrderId($incrementId)
                    ->setLastRealOrderId($incrementId);

                //->clearHelperData();
               
                
                // Log order created message
                //echo('Order created with increment id: '.$incrementId);
                
                
                $sampel = Mage::getSingleton('checkout/session')->getLastRealOrderId();

        
                //echo $sampel;
                //var_dump($sampel);     
                $result['success'] = true;
                $result['error']   = false;
                
                //$redirectUrl = Mage::getSingleton('checkout/session')->getRedirectUrl();
                //$redirectUrl = Mage::getUrl('checkout/onepage/success');
                //$result['redirect'] = $redirectUrl;

                $redirecturl = Mage::getBaseUrl().'snap/payment/redirect';
                
                // Show response
                $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
                $this->getResponse()->setRedirect($redirecturl);
                $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode($result));
                  
                
            } catch (Mage_Core_Exception $e) {
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $e->getMessage();    
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                    }
                }
            } catch (Exception $e) {
                $result['success']  = false;
                $result['error']    = true;
                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                Mage::logException($e);
                //$this->_goBack();
            }
            
            

            
        }

        public function sendNewOrderMail($customer, $order, $productid, $template)
        {
            $mail = Mage::getModel('core/email');
            $mail->setToName($customer->getName());
            $mail->setToEmail($customer->getEmail());
            
            
            $mail->setBody('Your new order ID is #' . $order->getRealOrderId());
            $mail->setSubject('New Order');

            $mail->setFromEmail('contact@wearinasia.com');
            $mail->setFromName('Wearinasia');
            $mail->setType('html');

            try {
                $mail->send();
                Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
                return true;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError('Unable to send.');
                return $e->getMessage();
            }
        }


        public function jsonAction(){
            $order = Mage::getModel('sales/order')->load(11533);
            $shipping = $order->getShippingMethod();
            echo $order->getStatus();
            echo $order->getIncrementId();
            var_dump($shipping);
        }

            public function sendAction()
            {
            $customer = Mage::getSingleton('customer/session')->getCustomer();

            $templateId = 38; // Enter you new template ID
            $senderName = Mage::getStoreConfig('trans_email/ident_support/name');  //Get Sender Name from Store Email Addresses
            $senderEmail = Mage::getStoreConfig('trans_email/ident_support/email');  //Get Sender Email Id from Store Email Addresses
            $sender = array('name' => $senderName,
                        'email' => $senderEmail);

            // Set recepient information
            $recepientEmail = $customer->getEmail();
            $recepientName = $customer->getName();      

            // Get Store ID     
            $store = Mage::app()->getStore()->getId();

            // Set variables that can be used in email template
            $vars = array(
                'customerName' => $customer->getName(),
                'emailTitle' => 'New Order',
                'productName' => 'Product Name',
                'bookingId' => 'BookingId',
                'bookingId' => 'BookingId',
            );  


            // Send Transactional Email
                Mage::getModel('core/email_template')
                    ->sendTransactional($templateId, $sender, $recepientEmail, $recepientName, $vars, $storeId);

                Mage::getSingleton('core/session')->addSuccess($this->__('We Will Contact You Very Soon.'));
                }
            
            
            
    }
        
            

?>