<?php 

    class Bisnis_Exp_BookorderController extends Mage_Core_Controller_Front_Action {
        
        
        public function textAction(){

            //$collection = Mage::getModel('catalog/product')->getCollection();
            $_productCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('exp_host_id', '1')
            ->addAttributeToSort('created_at', 'DESC');


        }
        
        protected function _clearsession(){
            // Clear Magento cart and quote items
            $cart = Mage::getModel('checkout/cart');
            $cart->truncate()->save(); // remove all active items in cart page
            $cart->init();
            $session= Mage::getSingleton('checkout/session');
            $quote = $session->getQuote();
            $cart = Mage::getModel('checkout/cart');
            $cartItems = $cart->getItems();
            foreach ($cartItems as $item)
            {
            $quote->removeItem($item->getId())->save();
            }
            Mage::getSingleton('checkout/session')->clear();
            
        }

        public function IndexAction(){
            if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                echo('mohon login terlebih dahulu');
            }else{
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('Book Order');
                $this->renderLayout();
            }
        }
        //buat booking , dipake di formbookorder 
        public function addOrderAction(){
            $params = $this->getRequest()->getParams();
            $store = Mage::app()->getStore();
            $website = Mage::app()->getWebsite();
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            //var_dump($params);

            $namaparams = $params['name'];
            $emailparams = $params['email'];
            $bookdateparams = $params['bookdate'];
            
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();


            // json dummy 
            $json = '[
                {
                    "name": "Professional JavaScript",
                    "email": "Nicholas C. Zakas",
                    "phone" : "09109"
                },
                {
                    "name": "JavaScript: The Definitive Guide",
                    "email": "David Flanagan",
                    "phone" : "1231"
                },
                {
                    "name": "High Performance JavaScript",
                    "email": "Nicholas C. Zakas",
                    "phone" : "12313"
                }
            ]';

            $dekot = json_decode($json, true);
            foreach($dekot as $dek){
                //$x .= implode(' ', $dek);
                $keterangan .= $dek['name'].$dek['email'].$dek['phone'];
            }
            // $user  = Mage::getSingleton('customer/session');
            
            
            
            
            // $firstName = $namaparams;
            // $lastName = $namaparams;
            // $email = $emailparams;

            // information akan menjadi json untuk disimpan
            $information = array(  
                'guest1' => array(
                    'name' => 'guestname', // compulsory
                    'email' => 'guestemail', // optional
                    'phone' => 'guestphone'
                ),
                'guest2' => array(
                    'name' => 'guestname2', // compulsory
                    'email' => 'guestemail2', // optional
                    'phone' => 'guestphone2'
                ),
            );

            $baru = json_encode($information, true);
            $billingAddress = array(
                        //'customer_address_id' => '',
                        'prefix' => '',
                        'firstname' => $firstName,
                        'middlename' => '',
                        'lastname' => $lastName,
                        'suffix' => '',
                        'company' => '', 
                        'street' => array(
                            '0' => 'No Address', // compulsory
                            
                        ),
                        'city' => 'No City',
                        'country_id' => 'ID', // two letters country code
                        'region' => 'Indonesia', // can be empty '' if no region
                        'region_id' => 'ID', // can be empty '' if no region_id
                        'postcode' => '0000',
                        'telephone' => '888-888-8888',
                        'fax' => '',
                        //'save_in_address_book' => 1
                    );

            
                    
            
                 
                    
            

            $shippingMethod = 'flatrate_flatrate';
            
            $paymentMethod = 'checkmo';
            // $productIds = array(3 => 1); 
            
            // Initialize sales quote object
            $quote = Mage::getModel('sales/quote')
                        ->setStoreId($store->getId());
            
            // Set currency for the quote
            $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
            
           
            
            
            
            // Assign customer to quote
            $quote->assignCustomer($customer);
            

            //tarik dari addtocart()
            $detail = Mage::getSingleton('customer/session')->getExpSession();
            $quantitycart = $detail['qty'];
            $productidcart = $detail['product'];
            //var_dump($quantitycart);

            //Add products to quote
            $product = Mage::getModel('catalog/product')->load($productidcart);
            $quote->addProduct($product, $quantitycart);
            
            // Add billing address to quote
            $billingAddressData = $quote->getBillingAddress()->addData($billingAddress);
            
            // Add shipping address to quote
            $shippingAddressData = $quote->getShippingAddress()->addData($billingAddress);
            
            // Collect shipping rates on quote shipping address data
            $shippingAddressData->setCollectShippingRates(true)
                                ->collectShippingRates();
            
            // Set shipping and payment method on quote shipping address data
            $shippingAddressData->setShippingMethod($shippingMethod)
                                ->setPaymentMethod($paymentMethod);
            
            // Set payment method for the quote
            $quote->getPayment()->importData(array('method' => $paymentMethod));
            
            try {
                // Collect totals of the quote
                $quote->collectTotals();
                $quote->setBookdate($bookdateparams);
                $quote->setInfoguest($baru);
                // Save quote
                $quote->save();
                
                // Create Order From Quote
                $service = Mage::getModel('sales/service_quote', $quote);
                $service->submitAll();
                $incrementId = $service->getOrder()->getRealOrderId();
                
                Mage::getSingleton('checkout/session')
                
                    ->setRealOrderId($incrementId)
                    ->setLastRealOrderId($incrementId);

                //->clearHelperData();
               
                
                // Log order created message
                echo('Order created with increment id: '.$incrementId);
                $sampel = Mage::getSingleton('checkout/session')->getLastRealOrderId();

        
                //echo $sampel;
                //var_dump($sampel);     
                $result['success'] = true;
                $result['error']   = false;
                
                //$redirectUrl = Mage::getSingleton('checkout/session')->getRedirectUrl();
                //$redirectUrl = Mage::getUrl('checkout/onepage/success');
                //$result['redirect'] = $redirectUrl;

                //$redirecturl = Mage::getBaseUrl().'snap/payment/redirect';
                
                // Show response
                $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
                $this->getResponse()->setRedirect($redirecturl);
                $this->getResponse()->setBody(
                Mage::helper('core')->jsonEncode($result));
                
                //Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getBaseUrl().'snapgopay/payment/redirect');
                //$this->_redirect('checkout/onepage/success', array('_secure'=>true));
                //$this->_redirect($redirectUrl);        
                
            } catch (Mage_Core_Exception $e) {
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $e->getMessage();    
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                    }
                }
            } catch (Exception $e) {
                $result['success']  = false;
                $result['error']    = true;
                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                Mage::logException($e);
                //$this->_goBack();
            }
            $this->_clearsession();
        }
        
        //buat ngecek order per hari (max and min qty) : sudah tidak terpakai , karena gabung di formbookorder
        public function selectorderbydateAction(){
            
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('select order by bookdate');
                $this->renderLayout();

        }
        // manggil bookorder detail
        public function bookingdetailAction(){
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('Book detail');
                $this->renderLayout();
        }
        //dipake untuk detail order == hoster bisa mulai experience saat udah mencet == ganti status 
        public function processcekinAction(){
            $params = $this->getRequest()->getParams();
            $getOrder = Mage::getModel('sales/order')->loadByIncrementId($params['incrementid']);
            $status = $getOrder->getStatus();
            
            if($status == "processing"){
                $order = Mage::getModel("sales/order")->loadByIncrementId($params['incrementid']);
                $order->setData('state', "processing");
                $order->setStatus("processing_checkin");
                $order->save();
                echo "request berhasil";
                $url = Mage::getUrl("exp/bookorder/index");
                Mage::app()->getResponse()
                    ->setRedirect($url, 301)
                    ->sendResponse();
            }
            
            if($status=="processing_checkin"){
                $order = Mage::getModel("sales/order")->loadByIncrementId($params['incrementid']);
                $order->setData('state', "complete");
                $order->setStatus("complete");
                $order->save();
                echo "request berhasil";
                $url = Mage::getUrl("exp/bookorder/index");
                Mage::app()->getResponse()
                    ->setRedirect($url, 301)
                    ->sendResponse();
            } 
            

            
        }
        // manggil formbookorder
        public function formbookorderAction(){
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('form-bookorder');
                $this->renderLayout();
        }
        //untuk sarana ngechat antara customer dan hoster
        public function chatorderAction(){
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('chat Order');
            $this->renderLayout();
        }
        //paramater orderid diambil dari order yang dipilih di phtml 
        public function kirimchatAction(){
            $params = $this->getRequest()->getParams();
            
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customerData->getId();
            $obj = $this->_cust = Mage::getModel('customer/customer')->load($id);
            
            $orderId = $params['orderid'];
            $order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            $nama = $obj->getFirstname();
            $time = Mage::getModel('core/date')->date('Y-m-d H:i:s');
            
            $pesan = $nama." : ".$params['pesan']. " " .$time ;
            $order->addStatusHistoryComment($pesan);
            $order->save();
            
            $url = Mage::getUrl("exp/inboxorder/index");
            Mage::app()->getResponse()
                ->setRedirect($url, 301)
                ->sendResponse();

        }
        // dipake di bookorder
        public function addtocartAction(){
            $prms = $this->getRequest()->getParams();
            $bookdate=$prms['bookdate'];
            $productid=$prms['productid'];
            $qty = $prms['qty'];

            $params = array(
                'product' => $productid,
                'qty' => $qty,
                'bookdate' => $bookdate,
            );

             
            $corses = Mage::getSingleton('customer/session');
            $corses->setExpSession($params);
             
            $url = Mage::getUrl("exp/bookorder/formbookorder");
            Mage::app()->getResponse()
                ->setRedirect($url, 301)
                ->sendResponse();
            //var_dump($params);


        }
        public function expsessionAction(){
            $corses = Mage::getSingleton('customer/session');
            var_dump($corses->getExpSession());
        }
        //untuk cek slot di selectorderbydate.phtml
        public function cekslotAction(){
            $params = $this->getRequest()->getParams();
            $tgl = $params['tanggal']; 
            $productid = $params['productid'];
            //echo $tgl;
            //var_dump($params);
            $start = $tgl.' 00:00:00';
            $end = $tgl.' 23:59:59';
            $from = Mage::getModel('core/date')->date($start);
            $now = Mage::getModel('core/date')->date($end);
            $orderan =  Mage::getResourceModel('sales/order_item_collection')
            ->addAttributeToFilter('created_at', array('from' => $from , 'to' => $now))
            ->addAttributeToFilter('product_id', $productid);
            
            $order_ids = $orderan->getColumnValues('order_id');        
           
            $countedArray = array_count_values($order_ids);
            $total = count($countedArray);
            
            $product = Mage::getModel("catalog/product")->load($productid);
            $tetapan = $product->getTetapanqty();
            $slot = $tetapan-$total;
            //untuk tampil didepan itu $slot
            echo $slot;

        }
        public function sendemailAction(){
            $emailTemplate = Mage::getModel('core/email_template')->loadDefault('recurring_order_email_template');

            //Getting the Store E-Mail Sender Name.
            $senderName = Mage::getStoreConfig('trans_email/ident_general/name');
            
            //Getting the Store General E-Mail.
            $senderEmail = Mage::getStoreConfig('trans_email/ident_general/email');
            
            //Variables for Confirmation Mail.
            $emailTemplateVariables = array();
            $emailTemplateVariables['name'] = "singgih";
            $emailTemplateVariables['email'] = "singgih@localhost";
            
            //Appending the Custom Variables to Template.
            $processedTemplate = $emailTemplate->getProcessedTemplate($emailTemplateVariables);
            
            //Sending E-Mail to Customers.
            $mail = Mage::getModel('core/email')
             ->setToName("ssd")
             ->setToEmail("ssd@localhost")
             ->setBody("hahahahahahahaha ini body")
             ->setSubject('Subject : halohalo halo')
             ->setFromEmail("singgih@localhost")
             ->setFromName("singgih")
             ->setType('html');
             try{
             //Confimation E-Mail Send
             $mail->send();
             }
             catch(Exception $error)
             {
             Mage::getSingleton('core/session')->addError($error->getMessage());
             return false;
             }            
        }

        






    }

?>