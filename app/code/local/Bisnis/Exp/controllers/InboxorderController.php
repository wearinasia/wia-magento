<?php 

    class Bisnis_Exp_InboxorderController extends Mage_Core_Controller_Front_Action {
        
        
        public function getSession()
        {
            $session = Mage::getSingleton('core/session');
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }

        public function validateSession()
        {
            $session = $this->getSession();
            $session_key = $this->getRequest()->getParam('key');  
            if ($session_key !== $session):
                $this->_redirect('');
                Mage::getSingleton('core/session')->addError('Your session is expired');
                return;
            endif;
        }

        public function validateCustomer(){
            $params = $this->getRequest()->getParams();
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            $login_customer_id = $customer->getId();

            $order = Mage::getModel('sales/order')->load($params['id']);
            $order_customer_id = $order->getCustomerId();

            if( $login_customer_id !== $order_customer_id){
                $this->_redirect('');
                Mage::getSingleton('core/session')->addError('Not Authorized');
                return;
            }
        }
        
        public function IndexAction(){

            if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                echo('mohon login terlebih dahulu');
            }else{
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('inbox Order');
                $this->renderLayout();
            }   
        }
        public function getCustomer(){
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customerData->getId();
            $obj = $this->_cust = Mage::getModel('customer/customer')->load($id);
            return $obj;
        }
        
        public function addAction(){

            $this->validateSession();
            $this->validateCustomer();
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('chat Order');
            $this->renderLayout();
        }

        public function sendAction(){
  
            $params = $this->getRequest()->getParams();
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customerId = $customerData->getId();
            
            $pesan = $params['pesan'];
            $orderId = $params['id'];
            $order = Mage::getModel('sales/order')->load($orderId);
            
                // $nama = $obj->getFirstname();
                $time = Mage::getModel('core/date')->date('Y-m-d H:i:s');
                // $pesan = $nama." : ".$params['pesan']. " " .$time ; 
                // $order->addStatusHistoryComment($pesan);
                // $order->save();


                $history = Mage::getModel('sales/order_status_history')
                ->setComment($params['pesan'])
                ->setStatus($order->getStatus())
                ->setEntityName($customerId)
                ->setCreatedAt($time);
                $order->addStatusHistory($history);
                $order->save();

                
                $this->_redirectReferer();
                $this->sendNewOrderMail($order,$pesan,$customerName,$orderId);
            
            
        }

        
        public function sendNewOrderMail($order,$pesan,$customerName,$orderId)
        {
            
            $mail = Mage::getModel('core/email');
            $mail->setToName('Customer Solution Wearinasia');
            $mail->setToEmail('cs@wearinasia.com');

           
            
            $url= 'https://wia.id/index.php/dapur/sales_order/view/order_id/'.$orderId;
            $balas = '<a href="'.$url.'">Balas Chat</a>';
            $mail->setSubject('Chat Baru dari nomor order' . $order->getRealOrderId());
            $mail->setBody('Anda menerima chat baru dari nomor order' . $order->getRealOrderId().'</br></br>'.'<div style="padding:1rem;border:1px solid #eee">'.$customerName.' : '.$pesan.'</div>'.$balas);

            $mail->setFromEmail('contact@wearinasia.com');
            $mail->setFromName('Wearinasia');
            $mail->setType('html');

            try {
                $mail->send();
                //Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
                return true;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError('Unable to send.');
                return $e->getMessage();
            }
        }
        






    }

?>