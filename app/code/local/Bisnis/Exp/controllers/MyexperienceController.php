<?php 

    class Bisnis_Exp_MyexperienceController extends Mage_Core_Controller_Front_Action {
        
        public function IndexAction(){

            if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                echo('mohon login terlebih dahulu');
            }else{
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('my experience');
                $this->renderLayout();
            }
        }

        //edit exp : form edit di phtml myexp
        public function editExpAction(){
            //echo "hai ini edit myexp";
            
            $params = $this->getRequest()->getParams();
            $sku= $params['sku'];
            $idproduk = $params['idproduk'];
            $namaproduk = $params['nama'];
            $deskripsi = $params['deskripsi'];
            $price = $params['harga'];
            $id = $params['id'];

            $modelproduk = Mage::getModel('catalog/product')->load($idproduk);
         
            $modelproduk->setName($namaproduk);
            $modelproduk->setSku($sku);
            $modelproduk->setDescription($deskripsi);
            $modelproduk->setPrice($price);
            $modelproduk->setCustomerId($id);
            $modelproduk->save();

        
            $url = Mage::getUrl("exp/myexperience/index");
            Mage::app()->getResponse()
                ->setRedirect($url, 301)
                ->sendResponse();
        
        }

        //add new exp
        public function addExpAction(){
            $params = $this->getRequest()->getParams();
            $namaproduct = $params['nama'];
            $sku = $params['sku'];
            $deskripsi = $params['deskripsi'];
            $harga = $params['harga'];
            $qty = $params['qty'];
            $id = $params['id'];
            //var_dump($params);

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel("catalog/product");
            $product
                ->setStoreId(0) 
                ->setWebsiteIds(array(1)) 
                ->setAttributeSetId(4) 
                ->setTypeId('simple') 
                ->setCreatedAt(strtotime('now')) 
                ->setSku($sku)
                ->setName($namaproduct)
                ->setWeight(4.0000)
                ->setStatus(1)
                ->setTaxClassId(4)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->setManufacturer(28)
                ->setColor(24)
                ->setNewsFromDate('06/26/2014')
                ->setNewsToDate('06/30/2016')
                ->setCountryOfManufacture('AF')            
                ->setPrice($harga)
                ->setCost(22.33)
                ->setSpecialPrice(00.44)
                ->setSpecialFromDate('06/1/2015')
                ->setSpecialToDate('06/30/2016')
                ->setMsrpEnabled(1)
                ->setMsrpDisplayActualPriceType(1)
                ->setMsrp(99.99)            
                ->setMetaTitle('test meta title 2')
                ->setMetaKeyword('test meta keyword 2')
                ->setMetaDescription('test meta description 2')            
                ->setDescription($deskripsi)
                ->setShortDescription('This is a short description')
                ->setCustomerId($id)

                    // ->setMediaGallery (array('images'=>array (), 'values'=>array ()))
                    // ->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false)

            ->setStockData(array(
                            'use_config_manage_stock' => 0,
                            'manage_stock'=>1,
                            'min_sale_qty'=>1,
                            'max_sale_qty'=>2,
                            'is_in_stock' => 1,
                            'qty' => $qty
                        )
            )

            ->setCategoryIds(array(3, 7))
            //harusnya ini dari parameter form nanti pas milih tanggal 
            ->setJanuariCoba(array(24,23)) 
            ->setTetapanqty(10);
            $product->save();
            //echo "berhasil";
            $url = Mage::getUrl("exp/myexperience/index");
            Mage::app()->getResponse()
                ->setRedirect($url, 301)
                ->sendResponse();

        }

        public function setscheduleAction(){
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('my experience - set schedule');
            $this->renderLayout();
        }

        public function setdateproductAction(){
            $params = $this->getRequest()->getParams();
//            $date = $params['date'];
            $information =$params['date'];
            //$baru = json_encode($information, true);


            $idproduk = $params['idproduk'];
            $modelproduk = Mage::getModel('catalog/product')->load($idproduk);
            $modelproduk->setDateproduct($information);
            $modelproduk->save();
            echo "success";
            $url = Mage::getUrl("exp/myexperience/index");
            Mage::app()->getResponse()
                ->setRedirect($url, 301)
                ->sendResponse();

        }





    }

?>