<?php 

    class Bisnis_Exp_PayoutController extends Mage_Core_Controller_Front_Action {
        
        public function IndexAction(){

            if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                echo('mohon login terlebih dahulu');
            }else{
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('payout');
                $this->renderLayout();
            }
        }

        public function getCustomer(){
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customerData->getId();
            $obj = $this->_cust = Mage::getModel('customer/customer')->load($id);
            return $obj;
        }

        public function processAction(){
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('payout');
            $this->renderLayout();
        }

        public function paidAction(){
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('payout');
            $this->renderLayout();
        }
        //buat hoster minta request pembayaran 
        public function editAction(){

            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customer_id = $customerData->getId();
            $params = $this->getRequest()->getParams();
            $id = $params['id'];
            $orderid =$params['orderid'];
            $order = Mage::getModel("sales/order")->loadByIncrementId($orderid);
            
            if($id == $customer_id){
                if($order->getStatus()=="complete"){
                    $order->setData('state', "complete");
                    $order->setStatus("complete_requestpayout");
                    $order->save();

                    $url = Mage::getUrl("exp/payout/index");
                    Mage::app()->getResponse()
                        ->setRedirect($url, 301)
                        ->sendResponse();
                }
            }else{
                $this->_redirect('*/*/');
                Mage::getSingleton("core/session")->addSuccess('tidak ada akses untuk mengedit'); 
                
            }


           

            
        }
        //buat liat siapa aja yang minta request via admin
        public function adminpayoutrequestAction(){
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customerData->getId();
            if($id!=3){
                $this->_redirect('*/*/');
                Mage::getSingleton("core/session")->addSuccess('Not Authorized'); 
            }else{
                //kasih kondisi buat akun akun tertentu aja , as admin ( ka anti, ko albert , dst  )
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('payout request via admin');
                $this->renderLayout();
            }

            
        }

        public function paidrequestAction(){
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $id = $customerData->getId();
            if($id!=3){
                $this->_redirect('*/*/');
                Mage::getSingleton("core/session")->addSuccess('Not Authorized'); 
            }else{
                $params = $this->getRequest()->getParams();
                //var_dump($params);
                $order = Mage::getModel("sales/order")->loadByIncrementId($params['orderid']);
                $order->setData('state', "complete");
                $order->setStatus("complete_paid");
                $order->save();
                echo "request berhasil";
    
                $url = Mage::getUrl("exp/payout/adminpayoutrequest");
                Mage::app()->getResponse()
                    ->setRedirect($url, 301)
                    ->sendResponse();
            }
        }



    }

?>