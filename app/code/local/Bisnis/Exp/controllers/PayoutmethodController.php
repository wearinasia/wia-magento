<?php 

    class Bisnis_Exp_PayoutmethodController extends Mage_Core_Controller_Front_Action {
        
        public function IndexAction(){
            if(!Mage::getSingleton('customer/session')->isLoggedIn()){
                echo('mohon login terlebih dahulu');
            }else{
                $this->loadLayout();
                $this->getLayout()->getBlock('head')->setTitle('Book Order');
                $this->renderLayout();
            }
        }

        public function addpayoutmethodAction(){
            $params = $this->getRequest()->getParams();
            $id = $params['idsession'];

            $payoutmodel = Mage::getModel('exp/account')->getCollection();
            $payoutmodel->addFieldToSelect('customer_id', $id);
            $kumpulanid [] = array ();

            foreach($payoutmodel as $data) {
                $kumpulanid[] = $data['customer_id'];
            }

            if (in_array($id, $kumpulanid)){
                echo "gabisa daftar lagi" ;
                $url = Mage::getUrl("exp/payoutmethod/index");
                Mage::app()->getResponse()
                    ->setRedirect($url, 301)
                    ->sendResponse();

            }else{
                $modelaccount = Mage::getModel('exp/account');
                $namaakun = $params['acc_name'];
                $nomorakun = $params['acc_number'];
                $namabank = $params['bank_name'];
                $modelaccount->setAccName($namaakun);
                $modelaccount->setAccNumber($nomorakun);
                $modelaccount->setBankName($namabank);
                $modelaccount->setCustomerId($id);
                $modelaccount->save();
                echo "berhasil" ;

                $url = Mage::getUrl("exp/payoutmethod/index");
                Mage::app()->getResponse()
                    ->setRedirect($url, 301)
                    ->sendResponse();
                

            }
        }

        public function editpayoutmethodAction(){
            
            $params = $this->getRequest()->getParams();
            $namaakun = $params['acc_name'];
            $nomorakun = $params['acc_number'];
            $namabank = $params['bank_name'];
            $id = $params['idsession'];

            $modelaccount = Mage::getModel('exp/account')->getCollection();
  
            foreach ($modelaccount as $data) {
                if($data['customer_id']== $id){
                    $data->setAccName($namaakun);
                    $data->setAccNumber($nomorakun);
                    $data->setBankName($namabank);
                    $data->save();
                }
            }
            echo "bisa update";
            $url = Mage::getUrl("exp/payoutmethod/index");
            Mage::app()->getResponse()
                ->setRedirect($url, 301)
                ->sendResponse();

        }

        public function formaddpayoutmethodAction(){
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('form-payoutmethod');
            $this->renderLayout();
        }

        public function formeditpayoutmethodAction(){
            $this->loadLayout();
            $this->getLayout()->getBlock('head')->setTitle('form-payoutmethod');
            $this->renderLayout();
        }


        






    }

?>