<?php 

    class Bisnis_Exp_ProductController extends Mage_Core_Controller_Front_Action {
        
        public function _getSession()
        {
            $session = Mage::getSingleton('core/session');
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }

       
        
        
        // render product page
        public function indexAction()
        {
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->renderLayout();
        }

        // render add page
        public function onboardAction()
        {
          

            $this->loadLayout();
            $this->renderLayout();
        }

        // render add page
        public function addAction()
        {
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->renderLayout();
        }

        // render edit page
        public function editAction()
        {
            if (!Mage::getSingleton('customer/session')->isLoggedIn()):
                $this->_redirect('customer/account/login');
                return;
            endif;

            $this->loadLayout();
            $this->renderLayout();
        }

        // POST Add product form
        public function addPostAction()
        {
            
            if (!$this->_validateFormKey()) {
                $this->_redirect('*/*');
                return;
            }

            
            $this->updateCustomerProfile();

            $params = $this->getRequest()->getParams();
            $namaproduct = $params['title'];
            $desc = $params['desc'];
            $short_desc = $params['short_desc'];
            $qty = $params['qty'];
            $id = $params['customer_id'];
            //var_dump($params);

            $price = str_replace(',','',$params['price']);

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel("catalog/product");
            $itenarary = json_encode($params['itenerary']);


            $date = json_encode(

                $array = array (Mage::getModel('core/date')->date('Y-m-d'))
            );

            try{
                $product
                ->setStoreId(0) 
                ->setWebsiteIds(array(1)) 
                ->setAttributeSetId(15) 
                ->setTypeId('simple') 
                ->setCreatedAt(strtotime('now')) 
                ->setSku($id.'-'.strtotime('now'))
                ->setName($namaproduct)
                ->setWeight(0)
                ->setStatus(1)
                ->setTaxClassId(0)
                ->setVisibility(1)         
                ->setPrice($price)
                
                ->setDescription($params['description'])
                ->setShortDescription($params['short_description'])
                ->setCustomerId($id)
                ->setExpAvailableDate($date)
                ->setCategoryIds(array($params['category']))
                ->setExpInclude(json_encode($params['include']))
                    ->setExpExclude(json_encode($params['exclude']))
                    ->setExpLocation($params['exp_location'])
                    ->setExpDuration($params['exp_duration'])
                    ->setExpItenenary($itenarary)

                
             
                ->setCustomDesign('wia-new/experience')
                
                    // ->setMediaGallery (array('images'=>array (), 'values'=>array ()))
                //->addImageToMediaGallery('media/host/4094-0.jpg', array('image','thumbnail','small_image'), false, false)

                ->setStockData(array(
                            'use_config_manage_stock' => 0,
                            'manage_stock'=>0,
                            
                           
                        )
                )

            
                //->setJanuariCoba(array(24,23)) 
                ->setTetapanqty($qty);
                $product->save();

                $new_product = $product->getId();
               
                //$this->upload($new_product);

                Mage::getSingleton("core/session")->addSuccess("you have successfully edit your service"); 
                //$this->_redirect('*/*/');

                $param = array(
                    // add  ? mark to urls
                    'id'   => $new_product  , //parameter as store
                     'image'=>'1' //parameter as category id
                       
                   );
                $this->_redirect('exp/product/edit/', $param );

                //send notif to admin
                
                }
           
                catch(Exception $e){
                Mage::log($e->getMessage());
                Mage::getSingleton("core/session")->addError("fail"); 
                $this->_redirectReferer();
                    
                }

                $this->sendNewOrderMail($new_product);


                

                
        }
            
            

        

        // POST Edit product form
        public function editPostAction()
        {

            
            if (!$this->_validateFormKey()) {
                $this->_redirect('*/*');
                return;
            }
            $this->updateCustomerProfile();
            
            $params = $this->getRequest()->getParams();
            $post_product_id = $params['id'];
            $post_customer_id = $params['customer_id'];
            $post_session_id = $params['session_id'];

            
            $price = str_replace(',','',$params['price']);
            
            //Load product
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel('catalog/product')->load($post_product_id);
            $customer_id = $product->getData('customer_id');

           $itenarary = json_encode($params['itenerary']);

           //echo $itenarary;

            
           
            if($post_customer_id == $customer_id){
                try{
                    $product
                    ->setDescription($params['description'])
                    ->setShortDescription($params['short_description'])
                    ->setName($params['title'])
                    ->setExpInclude(json_encode($params['include']))
                    ->setExpExclude(json_encode($params['exclude']))
                    ->setExpLocation($params['exp_location'])
                    ->setExpDuration($params['exp_duration'])
                    ->setTetapanqty($params['qty'])
                    ->setExpItenenary($itenarary)
                    ->setPrice($price);
                    
                    $product->save();
                    Mage::getSingleton("core/session")->addSuccess("you have successfully edit your service"); 
                    
                    
                    $param = array(
                        // add  ? mark to urls
                        'id'   => $params['id'] , //parameter as store
                         'image'=>'1' //parameter as category id
                           
                       );
                    $this->_redirect('exp/product/edit/', $param );

                    
                    }
                    catch(Exception $e){
                    Mage::log($e->getMessage());
                        echo $e->getMessage();
                    }



            }
            else{
                $this->_redirect('*/*/');
                Mage::getSingleton("core/session")->addSuccess('Not Authorized'); 
                    
            }          
          
        }

        // POST Edit product form
        public function editSchedulePostAction()
        {

            
            if (!$this->_validateFormKey()) {
                $this->_redirect('*/*');
                return;
            }
            
            $params = $this->getRequest()->getParams();
            $post_product_id = $params['id'];
            $post_customer_id = $params['customer_id'];
            $post_session_id = $params['session_id'];
            $available_date = $params['available_date'];

            
            //Load product
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $product = Mage::getModel('catalog/product')->load($post_product_id);
            $customer_id = $product->getData('customer_id');
            //validate product owner & edit product
            if($post_customer_id == $customer_id){
                try{
                    $product
                    ->setExpAvailableDate($available_date);
                    $product->save();
                    Mage::getSingleton("core/session")->addSuccess("you have successfully edit your service"); 
                    $this->_redirect('*/*/*');
                    }
                    catch(Exception $e){
                    Mage::log($e->getMessage());
                        echo $e->getMessage();
                    }

            }
            else{
                $this->_redirect('*/*/');
                Mage::getSingleton("core/session")->addSuccess('Not Authorized'); 
                    
            }

            
            
            
                    
               
          
        }

        public function sendNewOrderMail($new_product)
        {
           
            // $new_product = 4082;
            // echo $new_product;

            $mail = Mage::getModel('core/email');
            $mail->setToName('sinta');
            $mail->setToEmail('sinta@wearinasia.com');
            
            $url= 'https://wia.id/index.php/dapur/catalog_product/edit/id/'. $new_product;
            $button = '<a href="'.$url.'">Review Experience</a>';
            
            $mail->setSubject('Experience baru');
            $mail->setBody('Ada penambahan experience baru, mohon untuk mereview</br></br>'.$button);
            $mail->setFromEmail('contact@wearinasia.com');
            $mail->setFromName('Wearinasia');
            $mail->setType('html');

            try {
                $mail->send();
                Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
                return true;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError('Unable to send.');
                return $e->getMessage();
            }
        }

        public function jsonAction(){
            // $customer = Mage::getModel('customer/customer')->load('23360');
            // $customer->setCustomerBio()->save();
            // $customer->setCustomerPhotoUrl()->save();
            // $customer->setCustomerJob()->save();

            // echo $customer->getCustomerBio();
            // echo $customer->getCustomerPhotoUrl();
            // echo $customer->getCustomerJob();

            
            
            //echo $customer->getHostDescription();
            //$attributes = Mage::getModel('customer/customer')->getAttributes();

            //$attributes = Mage::getModel('customer/customer')->getAttributes();
            //var_dump( $attributes);
            //$customer->setMyCustomAttribute("Some custom data that needs to be saved")->save();
            //echo $customer->get();
            //echo $customer->getRating();


//             $entityType     = 'customer';
// $attributeCode  = 'customer_description';
// $attribute      = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'image');
// var_dump($attribute);

// $customerObj  = Mage::getModel('customer/customer')->load('23360');
// $attr = $customerObj->getData('description');
// var_dump($attr);




            //     $attribute_code = '1';

            //         $eavCollections = Mage::getModel('eav/entity_attribute')->getCollection()
            //         ->addFieldToFilter('entity_type_id', array('eq'=> $attribute_code ));

            //     foreach($eavCollections as $eavCollection){
            //       $attribute_id = $eavCollection['attribute_code']; //get the attribute id
            //        var_dump($attribute_id);

            //    var_dump($eavCollection);

            //     }


            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                

            $productid='4068'; // Product id of that product which image you want to update
            //for remove existing Images
            $product = Mage::getModel('catalog/product')->load($productid);
            // $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
            // $mediaApiItems = $mediaApi->items($loadpro->getId());
            // var_dump($mediaApiItems);

            $_product = Mage::getModel('catalog/product')->load('2');
            $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
            try {
                $items = $mediaApi->items($_product->getId());
                foreach($items as $item) {
                    var_dump($item);
                       // echo ($mediaApi->remove($_product->getId(), $item['file']));
                    
                    
                }
                //echo ($mediaApi->remove($_product->getId(), '/s/c/screen_shot_2019-04-08_at_22.18.16_2_1.png'));
            } catch (Exception $exception){
                var_dump($exception);
                die('Exception Thrown');
            }

       

            // $product = Mage::getModel('catalog/product')->load(2);
            
            // var_dump($product->getMediaGalleryImages());

            // foreach ($product->getMediaGalleryImages() as $image){
            //  //var_dump($image);

            //    //$image->setFile('123');
            //    //$image->save();
            //    //echo $image->getFile();
            // }

           // $productMediaConfig = Mage::getModel('catalog/product_media')->getCollection();
            //var_dump($productMediaConfig );
            

        }

        public function uploadAction(){

                $params = $this->getRequest()->getParams(); 
                
                
                $file_dir  = Mage::getBaseDir('media') . DS . 'host' . DS ; // where we save images  
            
                Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                $product = Mage::getModel('catalog/product')->load($params['id']);
                $customer_id = $product->getData('customer_id');

                

                if ($data = $this->getRequest()->getParams()) {
                    $type = 'file';
                    //echo $_FILES['file']['name'];

                    foreach ($_FILES['file']['name'] as $key => $value){
                        if (isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
                        //echo $type;

                        
                        //echo $_FILES['file']['name'][$key];

                        try {
                            //$uploader = new Varien_File_Uploader($_FILES['file']['name'][$key]);
                            $uploader = new Varien_File_Uploader(
                                array(
                            'name' => $_FILES['file']['name'][$key],
                            'type' => $_FILES['file']['type'][$key],
                            'tmp_name' => $_FILES['file']['tmp_name'][$key],
                            'error' => $_FILES['file']['error'][$key],
                            'size' => $_FILES['file']['size'][$key]
                                )
                        );
                            


                            $uploader
                                ->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                            $uploader->setAllowRenameFiles(true);
                            $uploader->setFilesDispersion(true);
                            $path = Mage::getBaseDir('media') . DS . 'host';
                            if (!is_dir($path)) {
                                mkdir($path, 0777, true);
                            }
                           //$uploader->setData('');
                            $uploader->save($path, $_FILES['file']['name'][$key]);
                            
                            //$uploaded_path = $path;
                            $uploaded_name = $uploader->getUploadedFileName();
                            //$this->_saveCustomerFileData($filename);
                            //echo $uploaded_path.$uploaded_name;

                            

                            $product->addImageToMediaGallery($path.$uploaded_name, false , false, false);

                            $attributes = $product->getTypeInstance(true)->getSetAttributes($product);
                            $attributes['media_gallery']->getBackend()->updateImage($product, $uploaded_name, array('label' => 'host_upload'));
                                

                            

                        } catch (Exception $e) {
                            Mage::log($e->getMessage(), null, $this->_logFile);
                        }
                        


                    }

                    

                    }
                    
                }
                $product->save();
                
                $param = array(
                    // add  ? mark to urls
                    'id'   => $params['id'], //parameter as store
                    'image'=>'1', //parameter as category id
                       
                   );
                
                $this->_redirect('exp/product/edit/', $param );
           
        }

        public function upload2Action(){
            echo '123';

            // $params = $this->getRequest()->getParams(); 
            
            
            // $file_dir  = Mage::getBaseDir('media') . DS . 'host' . DS ; // where we save images  
        
            // Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            // $product = Mage::getModel('catalog/product')->load($params['id']);
            // $customer_id = $product->getData('customer_id');

            

            // if ($data = $this->getRequest()->getParams()) {
            //     $type = 'file';
            //     //echo $_FILES['file']['name'];

            //     foreach ($_FILES['file']['name'] as $key => $value){
            //         if (isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
            //         //echo $type;

                    
            //         //echo $_FILES['file']['name'][$key];

            //         try {
            //             //$uploader = new Varien_File_Uploader($_FILES['file']['name'][$key]);
            //             $uploader = new Varien_File_Uploader(
            //                 array(
            //             'name' => $_FILES['file']['name'][$key],
            //             'type' => $_FILES['file']['type'][$key],
            //             'tmp_name' => $_FILES['file']['tmp_name'][$key],
            //             'error' => $_FILES['file']['error'][$key],
            //             'size' => $_FILES['file']['size'][$key]
            //                 )
            //         );
                        


            //             $uploader
            //                 ->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            //             $uploader->setAllowRenameFiles(true);
            //             $uploader->setFilesDispersion(true);
            //             $path = Mage::getBaseDir('media') . DS . 'host';
            //             if (!is_dir($path)) {
            //                 mkdir($path, 0777, true);
            //             }
            //            //$uploader->setData('');
            //             $uploader->save($path, $_FILES['file']['name'][$key]);
                        
            //             //$uploaded_path = $path;
            //             $uploaded_name = $uploader->getUploadedFileName();
            //             //$this->_saveCustomerFileData($filename);
            //             //echo $uploaded_path.$uploaded_name;

                        

            //             $product->addImageToMediaGallery($path.$uploaded_name, false , false, false);

            //             $attributes = $product->getTypeInstance(true)->getSetAttributes($product);
            //             $attributes['media_gallery']->getBackend()->updateImage($product, $uploaded_name, array('label' => 'host_upload'));
                            

                        

            //         } catch (Exception $e) {
            //             Mage::log($e->getMessage(), null, $this->_logFile);
            //         }
                    


            //     }

                

            //     }
                
            // }
            // $product->save();
            
            // $param = array(
            //     // add  ? mark to urls
            //     'id'   => $params['id'], //parameter as store
            //     'image'=>'1', //parameter as category id
                   
            //    );
            
            
       
    }

        public function removeImageAction(){
            // $params = $this->getRequest()->getParams();
            
            // $_product = Mage::getModel('catalog/product')->load('2');
            // $mediaApi = Mage::getModel("catalog/product_attribute_media_api");

            // $items = $mediaApi->items($_product->getId());

            // var_dump($items);

            // $mediaApi->remove($_product->getId(), '/d/s/dsc03091-1600x1067-5.jpg');
            // $mediaApi->save();


            Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
            $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
            
            $items = $mediaApi->items('2');
           // var_dump($items);
            
            foreach($items as $item){
                // if ($gallery->getBackend()->getImage($product, $item['file'])) {
                //     $gallery->getBackend()->removeImage($product, $item['file']);
                // }
                echo $item['file'];

//                 $mediaApi->update(
//      '2',
//      $image['file'],
//      array("label" => 'Image label here')
//   );
                
                //var_dump($item);
            }

            //$product->save();
            


            // try {
            //     $items = $mediaApi->items($_product->getId());
            //     foreach($items as $item) {
            //        // echo $item['file'];
            //            echo ($mediaApi->remove($_product->getId(), '/d/s/dsc03091-1600x1067-5.jpg'));
                    
                    
            //     }
            //    // echo ($mediaApi->remove($_product->getId(), '/d/s/dsc03091-1600x1067-5.jpg'));
            // } catch (Exception $exception){
            //    // var_dump($exception);
            //    // die('Exception Thrown');
            // }

            
            
        }

      

        
        public function updateCustomerProfile(){

            $params = $this->getRequest()->getParams(); 
            $customer = Mage::getModel('customer/customer')->load($params['customer_id']);
            
            if($params['customer_bio'] !== null){
                $customer->setCustomerBio($params['customer_bio'])->save();
                //$customer->setCustomerPhotoUrl($params['customer_photo_url'])->save();
                $customer->setCustomerJob($params['customer_job'])->save();
            }
        }

       


    }

?>
