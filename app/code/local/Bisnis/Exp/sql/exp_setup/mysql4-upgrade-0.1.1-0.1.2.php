<?php
echo 'Testing our upgrade script (mysql4-upgrade-0.1.1-0.1.2.php) and NOT halting execution <br />';
    $installer = $this;
    $installer->startSetup();
    $installer->run("
    CREATE TABLE `{$installer->getTable('exp/schedule')}` (
    
    `schedule_id` int(11) NOT NULL auto_increment,
    `customer_id` int(10) UNSIGNED, 
    `product_id` int(10) UNSIGNED ,
    `date` datetime,

    PRIMARY KEY  (`schedule_id`),

    FOREIGN KEY (`customer_id`) REFERENCES customer_entity(`entity_id`) 
    ON DELETE CASCADE
    ON UPDATE CASCADE

    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  
    ALTER TABLE `schedules` ADD FOREIGN KEY (`product_id`) REFERENCES catalog_product_entity(`entity_id`);
    
    ");
    
    $installer->endSetup();




?>