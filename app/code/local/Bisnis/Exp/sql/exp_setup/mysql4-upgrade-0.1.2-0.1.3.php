<?php
echo 'Testing our upgrade script (mysql4-upgrade-0.1.2-0.1.3.php) and NOT halting execution <br />';
    $installer = $this;
    $installer->startSetup();
    $installer->run("
    CREATE TABLE `{$installer->getTable('exp/withdrawal')}` (
    `withdrawal_id` int(11) NOT NULL auto_increment,
    `customer_id` int(10) UNSIGNED, 
    `request_date` timestamp ,
    `status` int(1),
    `acc_id` int(11),
    `order_id` int(10) UNSIGNED,

    PRIMARY KEY (`withdrawal_id`),

    FOREIGN KEY (`customer_id`) REFERENCES customer_entity(`entity_id`) 
    ON DELETE CASCADE
    ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  
    ALTER TABLE `withdrawals` ADD FOREIGN KEY (`acc_id`) REFERENCES Accounts(`acc_id`);
    ALTER TABLE `withdrawals` ADD FOREIGN KEY (`order_id`) REFERENCES sales_flat_order(`entity_id`);
    
    ");
    
    $installer->endSetup();




?>