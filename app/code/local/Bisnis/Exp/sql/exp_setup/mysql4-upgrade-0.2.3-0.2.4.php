<?php
echo 'Testing our upgrade script (mysql4-upgrade-0.2.3-0.2.4.php) and NOT halting execution <br />';

$installer = new Mage_Sales_Model_Mysql4_Setup('core_setup');
$installer->startSetup();
    $attribute  = array(
            'type'          => 'text',
            'backend_type'  => 'text',
            'frontend_input' => 'text',
            'is_user_defined' => true,
            'label'         => 'infoguest',
            'visible'       => true,
            'required'      => false,
            'user_defined'  => false,   
            'searchable'    => false,
            'filterable'    => false,
            'comparable'    => false,
            'default'       => ''
    );
    $installer->addAttribute('order', 'infoguest', $attribute);
    $installer->endSetup();



?>