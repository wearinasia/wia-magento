<?php
echo 'Testing our upgrade script (mysql4-upgrade-0.2.5-0.2.6.php) and NOT halting execution <br />';
    $installer = new Mage_Sales_Model_Mysql4_Setup('core_setup');
    $installer->startSetup();
            $installer->addAttribute('catalog_product', 'customer_id', array(
                'group'           => 'general',
                'label'           => 'customer_id',
                'input'           => 'text',
                'type'            => 'int',
                'required'        => 0,
                'visible_on_front'=> 1,
                'filterable'      => 0,
                'searchable'      => 0,
                'comparable'      => 0,
                'user_defined'    => 1,
                'is_configurable' => 0,
                'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'note'            => '',
             ));
    $installer->endSetup();

?>
