<?php
echo 'Testing our upgrade script (mysql4-upgrade-0.2.6-0.2.7.php) and NOT halting execution <br />';
    $installer = new Mage_Eav_Model_Entity_Setup('core_setup');
            $installer->startSetup();
            $installer->addAttribute('catalog_product', 'tetapanqty', array(
                'group'           => 'general',
                'label'           => 'tetapanqty',
                'input'           => 'text',
                'type'            => 'int',
                'required'        => 0,
                'visible_on_front'=> 1,
                'filterable'      => 0,
                'searchable'      => 0,
                'comparable'      => 0,
                'user_defined'    => 1,
                'is_configurable' => 0,
                'global'          => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
                'note'            => '',
             ));

?>