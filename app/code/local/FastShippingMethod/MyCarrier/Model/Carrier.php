<?php

class FastShippingMethod_MyCarrier_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {


    protected $_code = 'fastshippingmethod_mycarrier';
    const IND_COUNTRY_ID = 'IDN';
    protected $rateResultFactory;
    protected $rateMethodFactory;

    

    public function collectRates(Mage_Shipping_Model_Rate_Request $request) 
    {
        $result = Mage::getModel('shipping/rate_result');
        
            $shipping_method = $this->APICall();
                //var_dump($shipping_method);
                foreach ($shipping_method as $items){
                    $shipping_method_code = $items['code'];
                    foreach($items['costs'] as $item){
                        $service = $item['service'];
                        $desc = $item['description'];
                        $price =  $item['cost'][0]['value'];
                        $est = $item['cost'][0]['etd'];
                        //echo $item['service'];
                        //var_dump($item);
                        $result->append($this->_getInstantShippingRate( $shipping_method_code, $service,$desc, $price, $est));
                    }

            }
        
        return $result;
    }
    public function helper($type = 'aongkir')
		{
			return Mage::helper($type);	
		}
    public function getCityId()
		{
			$string_city = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getCity();
			
			
			$sql = "select * from ".Mage::getConfig()->getTablePrefix()."daftar_alamat where concat(type,' ',city_name) = '$string_city' limit 0,1 ";
			
			$res =  $this->helper()->fetchSql($sql);
			
			return $res[0]['city_id'];
			
        }
        public function getOriginId()
		{
			return $this->helper()->config('origin');
        }
        
        public function getBeratTotal()
		{
			$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
			$totalWeight = 0;
			foreach($items as $item) {
				$totalWeight += ($item->getWeight() * $item->getQty()) ;
			}
			if($totalWeight < 1)
			$totalWeight = 1;
			return $totalWeight * 1000;
		}
        
    public function APICall($carierName){
            $dest = $this->getCityId();
            $origin = $this->getOriginId();
            $weight = $this->getBeratTotal();
            $curl = curl_init();
            
            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://pro.rajaongkir.com/api/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=".$origin."&originType=city&destination=".$dest."&destinationType=city&weight=".$weight."&courier=jne:sicepat",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key: 137c886a5d054c82d74e4a91098cac1b"
            ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            $array = Mage::helper('core')->jsondecode($response);

            if ($err) {
            echo "cURL Error #:" . $err;
            } else {
                
                //$shipping_method = $array['rajaongkir']['results'][0];
                $shipping_method = $array['rajaongkir']['results'];
                // $shipping_code = $array['rajaongkir']['results'][0]['code'];
                // $shipping_packet = $array['rajaongkir']['results'][0]['costs'];
                //var_dump($shipping_method);
                return $shipping_method;
            
            }

        }


    protected function _getInstantShippingRate($shipping_method_code, $service,$desc, $price, $est) {
        $upper = strtoupper($shipping_method_code.' - '.$service);
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($shipping_method_code.$service);
        $rate->setMethod('priceInstant'.$shipping_method_code.$service);
        $rate->setMethodTitle($upper);
        $rate->setMethodDescription($desc.' ( '.$est.' hari )');
        $rate->setPrice($price);
        $rate->setCost($price);   
        return $rate;
    }

    public function getAllowedMethods() {
        return array(
            'priceInstant' => 'Instant',
            
            
        );
    }

}
