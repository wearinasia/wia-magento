<?php

class GrabParcelMethod_MyCarrier_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {


    protected $_code = 'grabparcelmethod_mycarrier';
    const IND_COUNTRY_ID = 'IDN';
    protected $rateResultFactory;
    protected $rateMethodFactory;

    public function collectRates(Mage_Shipping_Model_Rate_Request $request) 
    {
		
		$result = Mage::getModel('shipping/rate_result');
        $expressWeightThreshold = $this->getConfigData('express_weight_threshold');
		$city = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getCity();
        

        //COD
        $restrictedCOD = array('Tangerang', 'Jakarta');
        foreach ($restrictedCOD as $value) {
            if(strstr($city, $value) == true)
            {
                if($city == 'Tangerang' || $value == 'Tangerang')
                {
                    $jarakC = $this->getConfigData('priceCOD');
                }
                if($city == 'Jakarta' || $value == 'Jakarta')
                {
                    $jarakC = $this->getConfigData('priceCOD');
                }
            }
        }

        //SAME DAY
        $restrictedSameDay = array('Tangerang');
        foreach ($restrictedSameDay as $value) {
            if(strstr($city, $value) == true)
            {
                if($city == 'Tangerang' || $value == 'Tangerang')
                {
                    $jarakS = $this->getConfigData('priceSameDay');
                }
            }
        }


        //NEXTDAY
        $restrictedNextDay = array('Tangerang', 'Bogor', 'Depok', 'Jakarta', 'Bekasi');
        foreach ($restrictedNextDay as $value) {
            if(strstr($city, $value) == true)
            {
                if($city == 'Tangerang' || $value == 'Tangerang' || $city == 'Bogor' || $value == 'Bogor' || $city == 'Depok' || $value == 'Depok' || $city == 'Jakarta' || $value == 'Jakarta' || $city == 'Bekasi' || $value == 'Bekasi')
                {
                    $jarakN = $this->getConfigData('priceNextDay');
                }
            }
        }

        
        //REGULAR
        $restrictedRegular = array('Tangerang', 'Bogor', 'Depok', 'Jakarta', 'Bekasi');
        foreach ($restrictedRegular as $value) {
            if(strstr($city, $value) == true)
            {
                if($city == 'Tangerang' || $value == 'Tangerang' || $city == 'Bogor' || $value == 'Bogor' || $city == 'Depok' || $value == 'Depok' || $city == 'Jakarta' || $value == 'Jakarta' || $city == 'Bekasi' || $value == 'Bekasi')
                {
                    $jarakR = $this->getConfigData('priceRegular');
                }
            }
        }

        $berat = $request->getPackageWeight();
        $priceN = $jarakN*$berat;
        $priceR = $jarakR*$berat;
        $priceS = $jarakS*$berat;
        $priceC = $jarakC*$berat;

        if($priceS)
        {
            $result->append($this->_getSameDayShippingRate($priceS));
        }
        
        if($priceR)
        {
            $result->append($this->_getRegularShippingRate($priceR));
        }
        
        if($priceN)
        {
            $result->append($this->_getNextDayShippingRate($priceN));
        }

        
        return $result;
    }


    protected function _getCODShippingRate($priceC) {
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('priceCOD');
        $rate->setMethodTitle('COD');
        $rate->setPrice($this->priceC = $priceC);
        $rate->setCost($this->priceC = $priceC);   
        return $rate;
    }

    protected function _getSameDayShippingRate($priceS) {
        $rate = Mage::getModel('shipping/rate_result_method');
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('priceSameDay');
        $rate->setMethodTitle('Sameday');
        $rate->setMethodDescription('Estimasi pengiriman : 3 - 6 jam kerja');
        $rate->setPrice($this->priceS = $priceS);
        $rate->setCost($this->priceS = $priceS);   
        return $rate;
    }

    protected function _getNextDayShippingRate($priceN) {
        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('priceNextDay');
        $rate->setMethodTitle('Nextday');
        $rate->setMethodDescription('Estimasi pengiriman : 1 hari kerja');
        $rate->setPrice($this->priceN = $priceN);
        $rate->setCost($this->priceN = $priceN);
        return $rate;
    }
    
    
    protected function _getRegularShippingRate($priceR)
    {
        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('priceRegular');
        $rate->setMethodTitle('Regular');
        $rate->setMethodDescription('Estimasi pengiriman : 1 - 2 hari kerja');     
        $rate->setPrice($this->priceR = $priceR);
        $rate->setCost($this->priceR = $priceR);
        return $rate;       
    }
    
    public function getAllowedMethods() {
        return array(
            'priceCOD' => 'COD',
            'priceSameDay' => 'Same Day',
            'priceNextDay' => 'Next Day',
            'priceRegular' => 'Regular',
        );
    }

}
