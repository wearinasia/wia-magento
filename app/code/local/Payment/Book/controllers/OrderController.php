<?php
    class Payment_Book_OrderController extends Mage_Core_Controller_Front_Action{

        public function indexAction()
        {
            

            $this->loadLayout();
            $this->renderLayout();
            
            
            
        }
        
        public function clearsession(){
            // Clear Magento cart and quote items
            $cart = Mage::getModel('checkout/cart');                
            $cart->truncate()->save(); // remove all active items in cart page
            $cart->init();
            $session= Mage::getSingleton('checkout/session');
            $quote = $session->getQuote();
            $cart = Mage::getModel('checkout/cart');
            $cartItems = $cart->getItems();
            foreach ($cartItems as $item)
            {
                $quote->removeItem($item->getId())->save();
            }
            Mage::getSingleton('checkout/session')->clear();
                        
        }

        
        public function addproductAction(){
            //clear cart first
            $this->clearsession();
            
            $params = $this->getRequest()->getParams();
            $productId = $params['product-id'];
            
                

        
            try{
                // Below code will create instance of cart
                $cart = Mage::getModel('checkout/cart');

                // This will initialize cart 
                $cart->init();

                // Get the product collection by passing product id 
                $productCollection = Mage::getModel('catalog/product')->load($productId);
                
                // Add product to cart 
                $cart-> addProduct($productCollection, array( 'product_id' => $productId, 'qty' => 1));
                
                // Save cart
                $cart->save();

                

                //$result['redirect'] = 'hello';
                $result['success'] = true;
                $result['error'] = false;

                $redirecturl = Mage::getBaseUrl().'bookings';
                //$response['response'] = $this->myAjax();
                $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
                $this->getResponse()->setRedirect($redirecturl);
                $this->getResponse()->setBody(
                    Mage::helper('core')->jsonEncode($result));
            }
            
            catch (Mage_Core_Exception $e){
                $result['success'] = false;
                $result['error'] = true;
            }

            
        }



        //create booking
        public function createbookingAction(){
            $store = Mage::app()->getStore();
            $website = Mage::app()->getWebsite();
            
            $params = $this->getRequest()->getParams();

            
            $formKeys = Mage::getSingleton('core/session')->getFormKey();

            $formkey = $params['form_key'];


           

            //Form Data
            $firstName = $params['firstname'];
            $lastName = $params['lastname'];
            $email = $params['email'];
            $phone = $params['phone'];
            
            $city = $params['city'];
            
            $street = $params['street'];
            $location = $params['location'];

            $payment = $params['selected-payment-method'];
           
            $date = $params['date'];
            $time = $params['time'];
            
            $billingAddress = array(
                        'customer_address_id' => '',
                        'prefix' => '',
                        'firstname' => $firstName,
                        'middlename' => '',
                        'lastname' => $lastName,
                        'suffix' => '',
                        'company' => '', 
                        'street' => array(
                            '0' => $street, // compulsory
                            '1' => $location // optional
                        ),
                        'city' => $city,
                        'country_id' => 'IDN', // two letters country code
                        'region' => $city, // can be empty '' if no region
                        'region_id' => '', // can be empty '' if no region_id
                        'postcode' => '0000',
                        'telephone' => $phone,
                        'fax' => '',
                        'save_in_address_book' => 1
                    );
                    
            $shippingAddress = array(
                        'customer_address_id' => '',
                        'prefix' => '',
                        'firstname' => $firstName,
                        'middlename' => '',
                        'lastname' => $lastName,
                        'suffix' => '',
                        'company' => '', 
                        'street' => array(
                            '0' => $street, // compulsory
                            '1' => $location // optional
                        ),
                        'city' => $city,
                        'country_id' => 'IDN', // two letters country code
                        'region' => $city, // can be empty '' if no region
                        'region_id' => '', // can be empty '' if no region_id
                        'postcode' => '0000',
                        'telephone' => $phone,
                        'fax' => '',
                        'save_in_address_book' => 1
                    );        
            
            /**
             * You need to enable this method from Magento admin
             * Other methods: tablerate_tablerate, freeshipping_freeshipping, etc.
             */ 
            $shippingMethod = 'flatrate_flatrate';
            
            /**
             * You need to enable this method from Magento admin
             * Other methods: checkmo, free, banktransfer, ccsave, purchaseorder, etc.
             */ 
            //$paymentMethod = 'snapgopay';
            
             //$paymentMethod = 'cashondelivery';
            $paymentMethod = $payment;
            /** 
             * Array of your product ids and quantity
             * array($productId => $qty)
             * In the array below, the product ids are 374 and 375 with quantity 3 and 1 respectively
             */ 
            $productIds = array(1 => 9); 
            
            // Initialize sales quote object
            $quote = Mage::getModel('sales/quote')
                        ->setStoreId($store->getId());


            $checkout_session = Mage::getSingleton('checkout/session');
            //$cq = $checkout_session->getQuote();
            //$cq->assignCustomer(Mage::getSingleton('customer/session')->getCustomer());
            $quote = Mage::getModel('sales/quote')->load($checkout_session->getQuoteId()); // Mage_Sales_Model_Quote
            $quote->getAllItems();                  
            $quote->reserveOrderId();   

            
            // Set currency for the quote
            $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
            
            $customer = Mage::getModel('customer/customer')
                            ->setWebsiteId($website->getId())
                            ->loadByEmail($email);
            
            /**
             * Setting up customer for the quote 
             * if the customer is not already registered
             */
            if (!$customer->getId()) {
                $customer = Mage::getModel('customer/customer');
                
                $customer->setWebsiteId($website->getId())
                        ->setStore($store)
                        ->setFirstname($firstName)
                        ->setLastname($lastName)
                        ->setEmail($email);             
                
                /**
                 * Creating new customer
                 * This is optional. You may or may not create/save new customer.
                 * If you don't need to create new customer, you may skip/remove the below try-catch blocks.
                 */                             
                try {
                    // you can write your custom password here instead of magento generated password
                    $password = $customer->generatePassword();         
                    $customer->setPassword($password);
                    
                    // set the customer as confirmed
                    $customer->setForceConfirmed(true);
                    
                    // save customer
                    $customer->save();
                    
                    $customer->setConfirmation(null);
                    $customer->save();
                    
                    // set customer address
                    $customerId = $customer->getId();        
                    $customAddress = Mage::getModel('customer/address');            
                    $customAddress->setData($billingAddress)
                                ->setCustomerId($customerId)
                                ->setIsDefaultBilling('1')
                                ->setIsDefaultShipping('1')
                                ->setSaveInAddressBook('1');
                    
                    // save customer address
                    $customAddress->save();
                    
                    // send new account email to customer    
                    //$customer->sendNewAccountEmail();
                    $storeId = $customer->getSendemailStoreId();
                    $customer->sendNewAccountEmail('registered', '', $storeId);
                    
                    // set password remainder email if the password is auto generated by magento
                    $customer->sendPasswordReminderEmail();
                    
                   
                    //Mage::log('Customer with email '.$email.' is successfully created.', null, $logFileName);
                    
                } catch (Mage_Core_Exception $e) {
                    if (Mage::getSingleton('customer/session')->getUseNotice(true)) {
                        Mage::getSingleton('customer/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                    } else {
                        $messages = array_unique(explode("\n", $e->getMessage()));
                        foreach ($messages as $message) {
                            Mage::getSingleton('customer/session')->addError(Mage::helper('core')->escapeHtml($message));
                        }
                    }
                } catch (Exception $e) {
                    //Zend_Debug::dump($e->getMessage());
                    Mage::getSingleton('customer/session')->addException($e, $this->__('Cannot add customer'));
                    Mage::logException($e);
                    //$this->_goBack();
                } 
            }
            
            // Assign customer to quote
            $quote->assignCustomer($customer);
                // Add billing address to quote
            $billingAddressData = $quote->getBillingAddress()->addData($billingAddress);
            
            // Add shipping address to quote
            $shippingAddressData = $quote->getShippingAddress()->addData($shippingAddress);
            
            /**
             * Billing or Shipping address for already registered customers can be fetched like below
             * 
             * $customerBillingAddress = $customer->getPrimaryBillingAddress();
             * $customerShippingAddress = $customer->getPrimaryShippingAddress();
             * 
             * Instead of the custom address, you can add these customer address to quote as well
             * 
             * $billingAddressData = $quote->getBillingAddress()->addData($customerBillingAddress);
             * $shippingAddressData = $quote->getShippingAddress()->addData($customerShippingAddress);
             */
            
            // Collect shipping rates on quote shipping address data
            $shippingAddressData->setCollectShippingRates(true)
                                ->collectShippingRates();
            
            // Set shipping and payment method on quote shipping address data
            $shippingAddressData->setShippingMethod($shippingMethod)
                                ->setPaymentMethod($paymentMethod);
            
            // Set payment method for the quote
            $quote->getPayment()->importData(array('method' => $paymentMethod));
            
            try {
               // Collect totals of the quote
               $quote->collectTotals();
            
               // Save quote
               $quote->save();
               
               // Create Order From Quote
               $service = Mage::getModel('sales/service_quote', $quote);
               $service->submitAll();
               $incrementId = $service->getOrder()->getRealOrderId();
               $orderId = $service->getOrder()->getId();
               

               Mage::getSingleton('checkout/session')
               //    ->setQuoteId('123')
               //    ->setLastRealOrderId('123');
                   ->setRealOrderId($incrementId)
                   ->setLastRealOrderId($incrementId);


                //set comment
                $order = Mage::getModel('sales/order')->load($orderId);
                $order->addStatusHistoryComment('Booking Date : '.$date.'</br>'.'Booking Time '.$time);
                $order->save();



               //->clearHelperData();
              
               /**
                * For more details about saving order
                * See saveOrder() function of app/code/core/Mage/Checkout/Onepage.php
                */ 
               
               //$result['redirect'] = 'hello';
            $result['success'] = true;
            $result['error'] = false;
            
            if($paymentMethod == 'snapgopay'){
                $redirecturl = Mage::getBaseUrl().'snapgopay/payment/redirect';
            }
            
            else{
                $redirecturl = Mage::getBaseUrl();
            }
                
            
            //$response['response'] = $this->myAjax();
            $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
            $this->getResponse()->setRedirect($redirecturl);
            $this->getResponse()->setBody(
            Mage::helper('core')->jsonEncode($result));

               
                
            } catch (Mage_Core_Exception $e) {
                //$redirecturl = Mage::getBaseUrl().'snapgopay/payment/redirect';
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $e->getMessage();    
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                    }
                }
            } catch (Exception $e) {
                $result['success']  = false;
                $result['error']    = true;
                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                Mage::logException($e);
                //$this->_goBack();
            } 


            //$quote = $customer = $service = null;

            //$this->clearsession();
            
             // auto login customer
             Mage::getSingleton('customer/session')->loginById($customer->getId());
                    
            
        
        
        }
        


    }

?>
