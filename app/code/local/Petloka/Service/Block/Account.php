<?php
class Petloka_Service_Block_Account extends Mage_Core_Block_Template
{
    
    public function getCustomerToken()
    {
        $session = Mage::getSingleton('customer/session');
        $token = $session->getToken();
        return $token;

    }

    public function getLoginCustomer()
    {
        $token = $this->getCustomerToken();
        

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        $userId =  $array['user_id'];
        return $userId;
       // var_dump($array);
        
        

    }

    

    public function getCustomerDetails()
    {
        
        $token = $this->getCustomerToken();
        

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        return $array;

    }
    
    

    

    public function getServiceDetails()
    {
        
        

        $token = $this->getCustomerToken();
        
        $config = json_encode(array(
            "service_type" => 'grooming',
           
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/grooming';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        curl_close($curl);
        return $array; 



        

    }


    public function getProductDetails()
    {
        
        
        $customer = $this->getCustomerDetails();
       
        $host_id = $customer['user_id'];

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/products?host='.$host_id;
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        return $array;


        

    }


   


    
	
	
	
    
}