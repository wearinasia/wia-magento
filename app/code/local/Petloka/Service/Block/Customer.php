<?php
class Petloka_Service_Block_Customer extends Mage_Core_Block_Template
{
    
    
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
    public function getCustomer()
    {
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if ($customer->getId()):
            return $customer;
        endif;

        return false;
    }
    
    public function getOrder()
    {
    
    	$orders = Mage::getModel('sales/order');
        return $orders;
    }
    
}

