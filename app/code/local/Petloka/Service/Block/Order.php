<?php
class Petloka_Service_Block_Order extends Mage_Core_Block_Template
{
    
    
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }

    public function getCustomerToken()
    {
        $session = Mage::getSingleton('customer/session');
        $token = $session->getToken();
        return $token;

    }
    
    public function getLoginCustomer()
    {
        $token = $this->getCustomerToken();
        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        $userId =  $array['user_id'];
        return $userId;
       // var_dump($array);
        
        

    }
    
    public function getOrderList(){

        $token = $this->getCustomerToken();
        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/mybookings';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);


        $collection = new Varien_Data_Collection();                
        foreach ($array as $item) {
            $varienObject = new Varien_Object();
            $varienObject->setBookingId($item['booking_id']);
            $varienObject->setProductName($item['product_name']);
            $varienObject->setStatus($item['status']);
            $varienObject->setBookingDate($item['start_time']);
            $varienObject->setAddress($item['booking_address']['street']);
            
            
            $collection->addItem($varienObject);
        }
        return $collection;

    }



    public function getOrderView(){
        $token = $this->getCustomerToken();
        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/bookings/5cd54d834d92a293ff01114d';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        var_dump($array);
        //return($array);

            $varienObject = new Varien_Object();
            $varienObject->setBookingId('123');
            // $varienObject->setProductName($item['product_name']);
            // $varienObject->setStatus($item['status']);
            // $varienObject->setBookingDate($item['start_time']);
            // $varienObject->setAddress($item['booking_address']['street']);
            //return $varienObject;

    }
}

