<?php
class Petloka_Service_Block_Product extends Mage_Core_Block_Template
{
    
    
    public function _prepareLayout()
    {
		return parent::_prepareLayout();
		
    }

    public function getCustomerToken()
    {
        $session = Mage::getSingleton('customer/session');
        $token = $session->getToken();
        return $token;

    }

    public function getHostCollectionAPI()
    {
        $params = $this->getRequest()->getParams();
        $serviceType = $params['service_type'];
        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/'.$serviceType;
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        
        return $array;
    }


    public function getHostCollection(){
        
        $datas = $this->getHostCollectionAPI();
        
        $collection = new Varien_Data_Collection();                
        foreach ($datas as $item) {
            $varienObject = new Varien_Object();
            $varienObject->setId($item['user_id']);
            $varienObject->setServiceType($item['service_type']);
            $varienObject->setPhoto($item['photo_url']);
            $varienObject->setPrice($item['min_price']);
            $varienObject->setServiceName($item['host_name']);
            $varienObject->setTagline($item['tagline']);
            $varienObject->setCompany($item['is_company']);
            $varienObject->setName($item['user_name']);
            $varienObject->setLocation($item['location']);

            
            $collection->addItem($varienObject);
        }
        return $collection;
    }
    



    public function getHostAPI($host_id){
        
        $params = $this->getRequest()->getParams();
        $host_id = $params['id'];

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/'.$host_id.'/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization: Bearer web_test_id"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
    
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        return $array;

    }

  
    function getHostDetail(){
        $params = $this->getRequest()->getParams();
        

        $data = $this->getHostAPI();
        
        $host = new Varien_Object();
        $host->setName($data['full_name']);
        $host->setBio($data['about_me']);
        $host->setPhoto($data['photo_url']);
        $host->setCity($data['address']['city']);
        $host->setServiceName($data['host']['grooming']['host_name']);
        $host->setServiceTagline($data['host']['grooming']['tagline']);
        $host->setPrice($params['min_price']);
        $host->setOperationals($data['host']['grooming']['operationalDays']);
        $host->setServicePhotos($data['host']['grooming']['photo_urls']);

        return $host;
        
        


    }

    function getHostDetails(){
        $data = $this->getHostAPI();
        
        return $data;
       
        
        


    }

    

    public function getProductId()

    {
        
        $params = $this->getRequest()->getParams();
        $product_id = $params['id'];

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/products?host='.$product_id;
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        return $array;
    }

    public function getCustomerDetails()
    {
        
        $token = $this->getCustomerToken();
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        return $array;

    }

    public function getServiceDetails()
    {
        
        

        $token = $this->getCustomerToken();
        $serviceType = $this->getRequest()->getParam('service_type');
        
        $config = json_encode(array(
            "service_type" => $serviceType,
           
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/'.$serviceType;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        curl_close($curl);
        return $array; 



        

    }

    public function getProductDetails()
    {
        
        
        $customer = $this->getCustomerDetails();
       
        $host_id = $customer['user_id'];

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/products?host='.$host_id;
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        return $array;


        

    }

    

    




   
    
	
	
	
    
}