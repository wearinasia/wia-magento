<?php
 
class Petloka_Service_AccountController extends Mage_Core_Controller_Front_Action
{
   
 
    public function getCustomerToken()
    {
        $session = Mage::getSingleton('customer/session');
        $token = $session->getToken();
        return $token;

    }
    
    public function getLoginCustomer()
    {
        $token = $this->getCustomerToken();
        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        $userId =  $array['user_id'];
        return $userId;
       // var_dump($array);
        
        

    }

    
    
    public function indexAction()
    {
       
        if ($this->getCustomerToken() == null):
            $this->_redirect('service/account/login');
            return;
        endif;
        $this->loadLayout();
        $this->renderLayout();
    }

    public function loginAction()
    {
        if ($this->getLoginCustomer() !== null):
            $this->_redirect('');
            return;
        endif;
       
        $this->loadLayout();
        $this->renderLayout();
    }

    public function registerAction()
    {
        if ($this->getLoginCustomer() !== null):
            $this->_redirect('');
            return;
        endif;
       
        $this->loadLayout();
        $this->renderLayout();
    }

    public function productAction()
    {
        if ($this->getLoginCustomer() == null):
            $this->_redirect('service/account/login');
            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }

    public function product_addAction()
    {
        if ($this->getLoginCustomer() == null):
            $this->_redirect('service/account/login');
            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }

    public function product_editAction()
    {
        if ($this->getCustomerToken() == null):
            $this->_redirect('service/account/login');
            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }

    public function savePersonalAction(){

        $token = $this->getCustomerToken();
        $param = $this->getRequest()->getParams();


        //var_dump($param);

        $config = json_encode($param);

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        curl_close($curl);
        //var_dump($array);
        //return $array; 
       $this->_redirect('service/account/product_add/step/2');
        
    }

    public function saveServiceAction(){

        $token = $this->getCustomerToken();
        
        $param = $this->getRequest()->getParams();

        if( $param['is_company'] == '1') {
            $company = true;
        }
        else{
            $company = false;
        }
        
        $config = json_encode(array(
            "service_type" => $param['service_type'],
            "host_name" => $param['host_name'],
            "count" => $param['count'],
            "tagline" => $param['tagline'],
            "is_company" => $company,
           
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/grooming';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 

        // var_dump($array);
        $this->_redirect('service/account/product_add/step/3');
    }

    public function saveProductAction(){

        $token = $this->getCustomerToken();
        
        $param = $this->getRequest()->getParams();

        $config = json_encode($param);

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/myproducts/grooming';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 

        //var_dump($array);
        $this->_redirect('service/account/product_add/step/3');
    }

    public function saveScheduleAction(){
        $token = $this->getCustomerToken();
        
        $param = $this->getRequest()->getParams();

        $operationalDays = $param['operationalDays'];
        $operationalHours = $param['operationalHours'];

        
        $config = json_encode(array(
            "service_type" => $param['service_type'],
            "operationalDays" => $operationalDays,
            "operationalHours" => $operationalHours,
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/grooming';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 

        //var_dump($array);
        $this->_redirect('service/account/product_add/step/5');
    }


    public function saveTokenAction(){
        
        $token = $this->getRequest()->getParam('code');
       
        $session = Mage::getSingleton('customer/session');
        $session->setToken($token);
        

        echo $session->getToken();
        

    }

    public function removeTokenAction(){
        
        //$token = $this->getRequest()->getParam('code');
       
        $session = Mage::getSingleton('customer/session');
        $session->setToken(null);

        echo $session->getToken();
        

    }

    public function SignInAction(){
        $param = $this->getRequest()->getParams();
        $config = json_encode(array(
            "email" => $param['email'],
            "password" => $param['password'],
            "returnSecureToken" => true,
        ));
        
        
        $url    = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        $code = $array['error']['code'];
        $message = $array['error']['message'];
        $token =  $array['idToken'];
        //var_dump($array);
        if($code == 400){
            $this->_redirectReferer();
            Mage::getSingleton("core/session")->addSuccess($message); 
                
        }
        else{
            $this->_saveToken($token);
            $this->_redirect('service/product/order/step/1');
            Mage::getSingleton("core/session")->addSuccess('Login Success'); 
        }
    }

    public function SignOutAction(){

        $session = Mage::getSingleton('customer/session');
        $session->setToken(null);
        $this->_redirectReferer();
        Mage::getSingleton("core/session")->addSuccess('Logout Success'); 

        //echo $session->getToken();
    }

    public function SignUpAction(){

        $param = $this->getRequest()->getParams();
        $config = json_encode(array(
            "email" => $param['email'],
            "password" => $param['password'],
            "returnSecureToken" => true,
        ));
        
        $url    = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        $code = $array['error']['code'];
        $message = $array['error']['message'];
        $token =  $array['idToken'];
        
        //var_dump($array); 
        

        if($code == 400){
            $this->_redirectReferer();
            Mage::getSingleton("core/session")->addSuccess($message); 
                
        }
        else{
            
            $this->_registerBackend($token);
            $this->_saveToken($token);
            $this->_redirect('service/product/order/step/1');
            Mage::getSingleton("core/session")->addSuccess('Login Success'); 
        }
        
        
               

      
    }

    public function _saveToken($token){
        
        $session = Mage::getSingleton('customer/session');
        $session->setToken($token);
        //echo $session->getToken();
        

    }

    public function _registerBackend($token){
        
        // $config = json_encode(array(
        //     "service_type" => $param['service_type'],
        //     "operationalDays" => $operationalDays,
        //     "operationalHours" => $operationalHours,
        // ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/authenticate';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        

    }

  

    public function saveUploadFileAction() {
        
        if ($data = $this->getRequest()->getParams()) {
            $type = 'file';
            //echo $_FILES['file']['name'];

            foreach ($_FILES['file']['name'] as $key => $value){
                if (isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
                //echo $type;

                
                //echo $_FILES['file']['name'][$key];

                try {
                    //$uploader = new Varien_File_Uploader($_FILES['file']['name'][$key]);
                    $uploader = new Varien_File_Uploader(
                        array(
                    'name' => $_FILES['file']['name'][$key],
                    'type' => $_FILES['file']['type'][$key],
                    'tmp_name' => $_FILES['file']['tmp_name'][$key],
                    'error' => $_FILES['file']['error'][$key],
                    'size' => $_FILES['file']['size'][$key]
                        )
                );
                    


                    $uploader
                        ->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);
                    $path = Mage::getBaseDir('media') . DS . 'petloka';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $uploader->save($path, $_FILES['file']['name'][$key]);
                    
                    //$uploaded_path = $path;
                    $uploaded_name = $uploader->getUploadedFileName();
                    $mediaPath = "https://petloka.com/media/petloka";

                    $imageArray[] = $mediaPath.$uploaded_name;
                    //$this->_saveCustomerFileData($filename);
                    //echo $uploaded_path.$uploaded_name;

                    

                    

                } catch (Exception $e) {
                    Mage::log($e->getMessage(), null, $this->_logFile);
                }
            
                


            }

            

            

            }
            
        }
        $this->_updateService($imageArray);
        
       // $this->_redirect('*/*/');
    }

    public function _updateService($imageArray){

        //var_dump($imageArray);
        //echo ('123');
        $token = $this->getCustomerToken();
        
        
        
        $config = json_encode(array(
            "service_type" => $param['service_type'],
            "photo_urls" => $imageArray,
           
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/grooming';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 

        // var_dump($array);
        $this->_redirect('service/account/product_add/step/5');
    }


    
   


    
    
    
    
   
     
    
}
?>