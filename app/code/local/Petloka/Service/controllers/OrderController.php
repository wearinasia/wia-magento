<?php
 
class Petloka_Service_OrderController extends Mage_Core_Controller_Front_Action
{
   
 
 	
    public function getCustomerToken()
    {
        $session = Mage::getSingleton('customer/session');
        $token = $session->getToken();
        return $token;

    }

    
    
    public function getLoginCustomer()
    {
        $token = $this->getCustomerToken();
        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        $userId =  $array['user_id'];
        return $userId;
       // var_dump($array);
        
        

    }

    

    


    
    
    public function listAction()
    {
       
        if ($this->getLoginCustomer() == null):
            $this->_redirect('service/account/login');
            Mage::getSingleton("core/session")->addSuccess('please login or create an account first'); 

            return;
        endif;
        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        if ($this->getLoginCustomer() == null):
            $this->_redirect('service/account/login');
            Mage::getSingleton("core/session")->addSuccess('please login or create an account first'); 

            return;
        endif;
        
        $this->loadLayout();
        $this->renderLayout();
    }
    
    
   
    
   
   
}
?>