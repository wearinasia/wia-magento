<?php
 
class Petloka_Service_ProductController extends Mage_Core_Controller_Front_Action
{
   
    public function getCustomerToken()
    {
        $session = Mage::getSingleton('customer/session');
        $token = $session->getToken();
        return $token;

    }

    public function getLoginCustomer()
    {
        $token = $this->getCustomerToken();
        

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        $userId =  $array['user_id'];
        return $userId;
       // var_dump($array);
        
        

    }
 	
    public function indexAction()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()):
            $this->_redirect('customer/account/login');
            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }
    
    public function addproductAction()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()):
            $this->_redirect('service/account/login');
            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }

    public function listAction()
    {
        

        $this->loadLayout();
        $this->renderLayout();
    }

    public function viewAction()
    {
        

        $this->loadLayout();
        $this->renderLayout();
    }

    public function addAction()
    {
        if ($this->getLoginCustomer() == null):
            $this->_redirect('service/account/login');
            Mage::getSingleton("core/session")->addSuccess('please login or create an account first'); 

            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }

    public function LoginAction()
    {
        

        $this->loadLayout();
        $this->renderLayout();
    }

    public function orderAction()
    {
       
        if ($this->getLoginCustomer() == null):
            $this->_redirect('service/account/login');
            Mage::getSingleton("core/session")->addSuccess('please login or create an account first'); 

            return;
        endif;

        $this->loadLayout();
        $this->renderLayout();
    }

    public function successAction()
    {
        

        $this->loadLayout();
        $this->renderLayout();
    }

    public function getHostAction()
    
    {
    	//$destination = "-6.246332,106.797712";
    	
    	
    	
    	$url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/grooming';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        //$array = Mage::helper('core')->jsondecode($result);
        var_dump ($result );
        
       
		


       	
        curl_close($curl); 


    }
    
    public function getProductAction()
    
    {
    	//$destination = "-6.246332,106.797712";
    	
    	
    	
    	$url    = 'https://spry-blade-228107.appspot.com/api/v1/products?host=1aKwQUzSYWVBQXickbSmcA5twSH3';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $arrays = Mage::helper('core')->jsondecode($result);
        curl_close($curl); 

        var_dump($arrays);
        
       


    }

    public function saveOrderAction(){
        $param = $this->getRequest()->getParams();
        //var_dump($param);
        $address = $param['address'];

        $token = $this->getCustomerToken();
        
        
        $config = json_encode(array(
            "address" => $address,
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        curl_close($curl);

        //var_dump( $array);

        $session = Mage::getSingleton('customer/session');
        $session->setCheckoutSession($param);

        
       
        $url = Mage::getUrl('service/product/order/step/2');
        Mage::app()->getResponse()
            ->setRedirect($url, 301)
            ->sendResponse();
    }

   
    
    public function createOrderAction()
    
    {
                
        
                
                $param = $this->getRequest()->getParams();
                $address = $param['booking_address'];

               // $this->updateCustomerAddress();

                $config = json_encode(array(
                    "start_time" => $param['booking_date'].$param['booking_time'],
                    "booking_address" => $address,
                    "note" => $param['note'],


                ));

                $product_id = $param['product_id'];

                $token = $this->getCustomerToken();

                $url    = 'https://spry-blade-228107.appspot.com/api/v1/products/'.$product_id.'/book';
    	        $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
                
                $result     = curl_exec($curl);
                $array = Mage::helper('core')->jsondecode($result);
                curl_close($curl); 
                

                
                if ($array['booking_id'] == null){
                    $url = Mage::getUrl('service/product/order/step/2');
                }
                else{
                    $lastbooking_params = array(
                        // add  ? mark to urls
                        "booking_id"   => $array['booking_id'], //parameter as store
                        "booking_date" => $param['booking_date'],
                        "booking_time" => $param['booking_time'],
                        "product_price" => $param['product_price'],
                        
                        
                           
                    );
    
    
    
                    $session = Mage::getSingleton('customer/session');
                    $session->setLastBookingSession($lastbooking_params);
                    $session->setCheckoutSession(null);
                    $session->setBookSession(null);
                    
                   
                    $url = Mage::getUrl('service/product/order/step/3');
                    Mage::app()->getResponse()
                        ->setRedirect($url, 301)
                        ->sendResponse();

                }

                
        

    }


    public function searchAction(){
        $param = $this->getRequest()->getParams();

       

        $this->_redirect('service/product/list',$param);
    }

    public function updateCustomerAddress(){
        
    }

    public function updateStatusAction()
    
    {
    	
                $config = json_encode(array(
                    "status" => 'PAYMENT_CONFIRMATION',
                    

                ));

                $booking_id = '5cb755c43b775000167ba2f0';

                $url    = 'https://spry-blade-228107.appspot.com/api/v1/bookings/'.$booking_id.'/status';
    	        $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_HEADER, false);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization: Bearer web_test_id"));
                curl_setopt($curl, CURLOPT_PUT, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
                
                $result     = curl_exec($curl);
                $array = Mage::helper('core')->jsondecode($result);
                curl_close($curl); 
                
                if( $array['booking_id'] !== null){

                    $params = array(
                        // add  ? mark to urls
                        'booking_id'   => $array['booking_id'], //parameter as store
                       
                           
                    );
                    
                    $this->_redirect('service/product/order/step/3/',$params);
                }


                //var_dump ($array);
        

    }



    

    
    public function addtocartAction(){
        //$this->validateSession();
        $product = $this->getRequest()->getParams();
         
        $session = Mage::getSingleton('customer/session');
        $session->setBookSession($product);
       
        $url = Mage::getUrl('service/product/order/step/1');
        Mage::app()->getResponse()
            ->setRedirect($url, 301)
            ->sendResponse();
        //var_dump($params);


    }

    public function reviewAction(){
        //$this->validateSession();
        $product = $this->getRequest()->getParams();
       // var_dump($product);
        // $session = Mage::getSingleton('customer/session');
        // $session->setBookSession($product);
        
       
        $url = Mage::getUrl('service/product/order/review/1');
        Mage::app()->getResponse()
            ->setRedirect($url, 301)
            ->sendResponse();
        //var_dump($params);


    }

    public function jsonAction(){
        
        $product_id = 'SKPYV8NrfERMFyjL88uqY15aR1E3';

        $url    = 'https://spry-blade-228107.appspot.com/api/v1/products?host=SKPYV8NrfERMFyjL88uqY15aR1E3&service_type=grooming';
    	$curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
  		curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
        curl_setopt($curl, CURLOPT_GET, true);
        curl_setopt($curl, CURLOPT_GETFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        var_dump($array);

         
    }


    //ADD SERVICE & PRODUCT

    public function savePersonalAction(){

        $token = $this->getCustomerToken();
        $param = $this->getRequest()->getParams();
        $serviceType = $param['service_type'];

        //var_dump($param);

        $config = json_encode($param);

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        curl_close($curl);
        //var_dump($array);
        //return $array; 
       // echo 'woi';
        $this->_redirect('service/product/add/service_type/'.$serviceType.'/step/2');
        
    }

    public function saveServiceAction(){

        $token = $this->getCustomerToken();
        $param = $this->getRequest()->getParams();
        $serviceType = $param['service_type'];
        

        if( $param['is_company'] == '1') {
            $company = true;
        }
        else{
            $company = false;
        }
        
        $config = json_encode(array(
            "service_type" => $param['service_type'],
            "host_name" => $param['host_name'],
            "count" => $param['count'],
            "tagline" => $param['tagline'],
            "is_company" => $company,
           
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/'.$param['service_type'];
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 
//echo $serviceType;
       //  var_dump($array);
      $this->_redirect('service/product/add/service_type/'.$serviceType.'/step/3');
    }

    public function saveProductAction(){

        $token = $this->getCustomerToken();
        
        $param = $this->getRequest()->getParams();

        $config = json_encode($param);

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/myproducts/'.$param['service_type'];
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 

        //var_dump($array);
        $this->_redirect('service/product/add/service_type/'.$param['service_type'].'/step/3');
    }

    public function saveScheduleAction(){
        $token = $this->getCustomerToken();
        
        $param = $this->getRequest()->getParams();

        $operationalDays = $param['operationalDays'];
        $operationalHours = $param['operationalHours'];

        
        $config = json_encode(array(
            "service_type" => $param['service_type'],
            "operationalDays" => $operationalDays,
            "operationalHours" => $operationalHours,
        ));

        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/'.$param['service_type'];
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 

        //var_dump($array);
        $this->_redirect('service/product/add/service_type/'.$param['service_type'].'/step/5');
    }

    public function saveUploadFileAction() {
        $param = $this->getRequest()->getParams();
        $serviceType = $param['service_type'];
        if ($data = $this->getRequest()->getParams()) {
            $type = 'file';
            //echo $_FILES['file']['name'];

            foreach ($_FILES['file']['name'] as $key => $value){
                if (isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
                //echo $type;

                
                //echo $_FILES['file']['name'][$key];

                try {
                    //$uploader = new Varien_File_Uploader($_FILES['file']['name'][$key]);
                    $uploader = new Varien_File_Uploader(
                        array(
                    'name' => $_FILES['file']['name'][$key],
                    'type' => $_FILES['file']['type'][$key],
                    'tmp_name' => $_FILES['file']['tmp_name'][$key],
                    'error' => $_FILES['file']['error'][$key],
                    'size' => $_FILES['file']['size'][$key]
                        )
                );
                    


                    $uploader
                        ->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);
                    $path = Mage::getBaseDir('media') . DS . 'petloka';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $uploader->save($path, $_FILES['file']['name'][$key]);
                    
                    //$uploaded_path = $path;
                    $uploaded_name = $uploader->getUploadedFileName();
                    $mediaPath = "https://petloka.com/media/petloka";

                    $imageArray[] = $mediaPath.$uploaded_name;
                    //$this->_saveCustomerFileData($filename);
                    //echo $uploaded_path.$uploaded_name;

                    

                    

                } catch (Exception $e) {
                    Mage::log($e->getMessage(), null, $this->_logFile);
                }
            
                


            }

            

            

            }
            
        }
        $this->_updateService($imageArray,$serviceType);
        
       // $this->_redirect('*/*/');
    }
    
    public function _updateService($imageArray,$serviceType){

        //var_dump($imageArray);
        //echo ('123');
        $token = $this->getCustomerToken();
        $config = json_encode(array(
            "photo_urls" => $imageArray,
        ));

        
        //$url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/hotel';
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/hosts/'.$serviceType;
        
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        // curl_close($curl); 

        // var_dump($array);
        $this->_redirect('service/product/add/service_type/'.$serviceType.'/step/5');
    }



    public function saveLogoAction() {
        $param = $this->getRequest()->getParams();
        $serviceType = $param['service_type'];
        if ($data = $this->getRequest()->getParams()) {
            $type = 'file';
            //echo $_FILES['file']['name'];

            foreach ($_FILES['file']['name'] as $key => $value){
                if (isset($_FILES[$type]['name']) && $_FILES[$type]['name'] != '') {
                //echo $type;

                
                //echo $_FILES['file']['name'][$key];

                try {
                    //$uploader = new Varien_File_Uploader($_FILES['file']['name'][$key]);
                    $uploader = new Varien_File_Uploader(
                        array(
                    'name' => $_FILES['file']['name'][$key],
                    'type' => $_FILES['file']['type'][$key],
                    'tmp_name' => $_FILES['file']['tmp_name'][$key],
                    'error' => $_FILES['file']['error'][$key],
                    'size' => $_FILES['file']['size'][$key]
                        )
                );
                    


                    $uploader
                        ->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(true);
                    $path = Mage::getBaseDir('media') . DS . 'petloka';
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $uploader->save($path, $_FILES['file']['name'][$key]);
                    
                    //$uploaded_path = $path;
                    $uploaded_name = $uploader->getUploadedFileName();
                    $mediaPath = "https://petloka.com/media/petloka";

                    $imageArray = $mediaPath.$uploaded_name;
                    //$this->_saveCustomerFileData($filename);
                    //echo $uploaded_path.$uploaded_name;

                    

                    

                } catch (Exception $e) {
                    Mage::log($e->getMessage(), null, $this->_logFile);
                }
            
                


            }

            

            

            }
            
        }
        $this->_updateLogo($imageArray,$serviceType);
        
       // $this->_redirect('*/*/');
    }
    
    public function _updateLogo($imageArray,$serviceType){

        $token = $this->getCustomerToken();
       
        //var_dump($param);

        $config = json_encode(array(
            "photo_url" => $imageArray,
        ));


        
        $url    = 'https://spry-blade-228107.appspot.com/api/v1/user/details';
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json","Authorization:".$token));
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config);
        
        $result     = curl_exec($curl);
        $array = Mage::helper('core')->jsondecode($result);
        curl_close($curl);
        //var_dump($array);
        //return $array; 
       // echo 'woi';
        $this->_redirect('service/product/add/service_type/'.$serviceType.'/step/5');
    }


   


    
    
    
    
   
     
    
}
?> 