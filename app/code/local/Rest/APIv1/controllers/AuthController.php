<?php

class Rest_APIv1_AuthController extends Mage_Core_Controller_Front_Action
{
    //Library Message Codes
    //405 Method Not Allowed
    //406 Headers error - Wrong Source or Authorization value
    //400 Error - No Source or Authorization value

    //401 Your session has expired due to inactivity. Please Login again.

    //200 Login Success - Send Token
    //403 Wrong Email or Password
    //403 Email and Password are required

    //200 Log Out Success
    //404 Can't Log Out - Can't read user/customer id

    //201 Register Success - RegisterAction
    //409 Email already exist in the system - RegisterAction

    //200 Reset Password Link Token has been send to your email
    //404 Fail - Email not found

    //200 Change Password Success
    //404 Failed to change password
    //409 Change Password Error - Old and New Password are same

    public function _sendAPI($data){

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }

    public function _getSession(){
        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()){
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }
        else {
            $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
            return $message_dialog;
        }
    }

    public function LoginAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 0){
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);

                $customerEmail = $params['email'];
                $customerPassword = $params['password'];

                if (!empty($customerEmail) && !empty($customerPassword)){

                    $customer = Mage::getModel("customer/customer"); 
                    $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                    $customer->loadByEmail($customerEmail); 

                    //Session
                    $session = Mage::getSingleton('customer/session');
                    //$customerId = $this->_getSession($customer);
                    
                    try{/*
                        if ($session->isLoggedIn()) {
                            $customerId = $this->_getSession();
                            $apiarray = array('token' => $customerId,
                                                'message_code' => '200 Login Success - Send Token');
                                        
                            $this->_sendAPI($apiarray);
                        }
                        else {
                            $session->start();
                            $session->setCustomerAsLoggedIn($customer);
                            $session->login($customerEmail, $customerPassword);
                            $customerId = $this->_getSession();

                            $apiarray = array('token' => $customerId,
                                                'message_code' => '200 Login Success - Send Token');
                                        
                            $this->_sendAPI($apiarray);
                        }*/

                        $session->start();
                            $session->setCustomerAsLoggedIn($customer);
                            $session->login($customerEmail, $customerPassword);
                            $customerId = $this->_getSession();

                            $apiarray = array('token' => $customerId,
                                                'message_code' => '200 Login Success - Send Token');
                                        
                            $this->_sendAPI($apiarray);
                        
                    }catch (Exception $e){
                        $message_code = array('message_code' => '403 Wrong Email or Password');
                        $this->_sendAPI($message_code);
                    } 
                } 
                else {
                    $message_code = array('message_code' => '403 Email and Password are required');
                        $this->_sendAPI($message_code);
                }
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

    /*
    public function LogoutAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation(); 
        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()) {
            $customerId = null;
            $session->logout();
            $message_code = array('message_code' => '200 Log Out Success');
            $this->_sendAPI($message_code);
        }
        else {
            $message_code = array('message_code' => "404 Can't Log Out - Can't read user/customer id");
            $this->_sendAPI($message_code);
        }
    }
    */

    public function RegisterAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 0){
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {

                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);

                $customerFirstName = $params['firstname'];
                $customerLastName = $params['lastname'];
                $customerEmail = $params['email'];
                $customerPassword = $params['password'];

                if (!empty($customerEmail) && !empty($customerPassword) && !empty($customerFirstName) && !empty($customerLastName)){
                    $customer = Mage::getModel('customer/customer');
                    $customer->setWebsiteId(Mage::app()->getWebsite()->getId());
                    $customer->loadByEmail($customerEmail);

                    if(!$customer->getId()) {
                        $customer->setEmail($customerEmail);
                        $customer->setFirstname($customerFirstName);
                        $customer->setLastname($customerLastName);
                        $customer->setPassword($customerPassword);

                        try{
                            $customer->save();

                            //Session
                            $session = Mage::getSingleton('customer/session');
                            $session->start();
                            $session->setCustomerAsLoggedIn($customer);
                            $session->login($customerEmail, $customerPassword);
                            $customerId = $this->_getSession($customer);
                            //$customerId = $customer->getId();

                            $apiarray = array('token' => $customerId, 'message_code' => '201 Register Success');
                            $this->_sendAPI($apiarray);     
                          }
                        catch (Exception $e) {
                            //Zend_Debug::dump($e->getMessage());
                        } 
                    }
                    else {
                        $message_code = array('message_code' => '409 Email already exist in the system');
                        $this->_sendAPI($message_code);
                    }
                }
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

    public function ForgotPasswordAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 0){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);
                $customerEmail = $params['email'];

                $customer = Mage::getModel("customer/customer"); 
                $customer->setWebsiteId(Mage::app()->getWebsite()->getId()); 
                $customer->loadByEmail($customerEmail); 

                $customerId = $customer->getId();
                if ($customerId) {
                    try {
                        $newResetPasswordLinkToken =  Mage::helper('customer')->generateResetPasswordLinkToken();
                        $customer->sendPasswordResetConfirmationEmail();
                        $message_code = array('message_code' => '200 Reset Password Link Token has been send to your email');
                        $this->_sendAPI($message_code);
                        //echo $newResetPasswordLinkToken;
                    } catch (Exception $exception) {
                        //Mage::log($exception);
                    }
                }
                else {
                    $message_code = array('message_code' => '404 Fail - Email not found');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

    public function ChangePasswordAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 0){
            if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                $getraws = $this->getRequest()->getRawBody();
                $params = json_decode($getraws, true);
                $customerEmail = $params['email'];
                $customerPassword = $params['password'];
                $customerNewPassword = $params['new_password'];
                //$customerToken = $params['token'];

                $session = Mage::getSingleton('customer/session');
                $customerId = $session->getId();
                $untukperbandingan = Mage::getModel('customer/customer')->load($customerId);
                $perbandinganemail = $untukperbandingan->getEmail();


                if($customerEmail == $perbandinganemail){
                    if($customerPassword != $customerNewPassword){
                        $idcustomer = $session->getId();
                        $customer = Mage::getModel('customer/customer')->load($idcustomer);
                                
                        try{
                            $customer->setPassword($customerNewPassword);
                            $customer->save();
                            
                            $message_code = array('message_code' => '200 Change Password Success');
                            $this->_sendAPI($message_code);
                        }
                        catch (Exception $e) {
                            $message_code = array('message_code' => '404 Failed to change password');
                            $this->_sendAPI($message_code);
                        } 
                    }
                    else {
                        $message_code = array('message_code' => '409 Change Password Error - Old and New Password are same');
                        $this->_sendAPI($message_code);
                    }
                }
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

    public function SessionAction(){
        /*
        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()){
            $SID = $session->getEncryptedSessionId();
            $array = array('token' => $SID);
            $this->_sendAPI($array);
        }
        else {
            $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
            $this->_sendAPI($message_dialog);
        }*/
        
    }
}
?>