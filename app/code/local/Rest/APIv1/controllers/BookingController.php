<?php

class Rest_APIv1_BookingController extends Mage_Core_Controller_Front_Action
{

    public function _sendAPI($data){
        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }

    public function _getSession(){
        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()){
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }
        else {
            $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
            return $message_dialog;
        }
    }

    public function IndexAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 1){
            //Get Parameter
            $params = $this->getRequest()->getParams();
            $getraws = $this->getRequest()->getRawBody();
            $rawbodys = json_decode($getraws, true);

            //Session
            $session = Mage::getSingleton('customer/session');

            if ($session->isLoggedIn()){
                $customer = $session->getId();
            }
            
            $booking = $params['booking'];
            $show = $params['show'];
            $token = $rawbodys['token'];

            if ($token == $this->_getSession()){
                if ($_SERVER['REQUEST_METHOD'] === 'GET'){
                    if(!empty($booking)){
                        $print = $this->_showBookingbyId($booking);
                        $this->_sendAPI($print);
                    }
                    if($show == 1){
                        $print = $this->_showOrdersbyProductId($customer);
                        $this->_sendAPI($print);
                    }
                    else {
                        $print = $this->_showBookingList($customer);
                        $this->_sendAPI($print);
                    }  
                }
                else if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                    $print = $this->_createBooking();
                    $this->_sendAPI($print);
                }
                else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){}
                else if ($_SERVER['REQUEST_METHOD'] === 'DELETE'){
                    $print = $this->_deleteBooking($booking);
                    $this->_sendAPI($print);
                }
                else {
                    $message_code = array('message_code' => '405 Method Not Allowed');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
                $this->_sendAPI($message_dialog);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        }    
    }

    public function _showBookingList($data){
        $customerId = $data;
        // $order = Mage::getResourceModel('sales/order_collection')
        //         ->addFieldToSelect('*')
        //         ->addFieldToFilter('customer_id', $customerId)
        //         ->setOrder('created_at', 'desc');
        // return $order;

        $orders = Mage::getModel("sales/order")->getCollection()->addFieldToFilter('customer_id', $customerId)->load();

        foreach ($orders as $order) {
            foreach ($order->getAllItems() as $item) {
                $productarray[] = array(
                    'id' => $order->getIncrementId(),
                    'product_id' => $item->getId(),
                    'name' => $item->getName(),
                    'customer_id' => $order->getCustomerId(),
                    'customer_name' => $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname(),
                    'customer_email' => $order->getCustomerEmail(),
                    'phone' => $order->getShippingAddress()->getTelephone(),
                    'booking_date' => $order->getCreatedAt(),
                    'qty' => $order->getTotalQtyOrdered(),
                    'status' => $order->getStatus(),
                    'sub_total' => $order->getSubtotal(),
                    'shipping_cost' => $order->getBaseShippingAmount(),
                    'voucher' => $order->getDiscountAmount(),
                    'base_grand_total' => $order->getBaseGrandTotal(),
                    'grand_total' => $order->getGrandTotal()
                );
            }
        }
        return $productarray;
    }

    public function _showBookingbyId($data){
        $orderId = $data;
        $order = Mage::getModel("sales/order")->loadByIncrementId($orderId);

        $customerId = $order->getCustomerId();

        $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addFieldToFilter('status', '1')
            ->addAttributeToFilter('visibility', 4)
            //->addAttributeToFilter('attribute_set_id', '15')
            ->addFieldToFilter('customer_id', $customerId)
            ->addAttributeToSort('created_at', 'DESC')
            ->addAttributeToSelect('*')
            ->setPageSize(20)
            ->load();
            
             foreach ($order->getAllItems() as $item) {
                $productarray = array(
                    'id' => $order->getId(),
                    'product_id' => $item->getId(),
                    'name' => $item->getName(),
                    'customer_id' => $order->getCustomerId(),
                    'customer_name' => $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname(),
                    'customer_email' => $order->getCustomerEmail(),
                    'phone' => $order->getShippingAddress()->getTelephone(),
                    'booking_date' => $order->getCreatedAt(),
                    'qty' => $order->getTotalQtyOrdered(),
                    'status' => $order->getStatus(),
                    'sub_total' => $order->getSubtotal(),
                    'shipping_cost' => $order->getBaseShippingAmount(),
                    'voucher' => $order->getDiscountAmount(),
                    'base_grand_total' => $order->getBaseGrandTotal(),
                    'grand_total' => $order->getGrandTotal()
                ); 
            }
        
        return $productarray;
    }

    public function _showOrdersbyProductId($data){
        $customerId = $data;
        $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addFieldToFilter('status', '1')
            ->addAttributeToFilter('visibility', 4)
            //->addAttributeToFilter('attribute_set_id', '15')
            ->addFieldToFilter('customer_id', $customerId)
            ->addAttributeToSort('created_at', 'DESC')
            ->addAttributeToSelect('*')
            ->setPageSize(20)
            ->load();

        foreach ($products as $product) {
            $productId = $product->getId();
            $collection = Mage::getResourceModel('sales/order_item_collection')
                ->addAttributeToFilter('product_id', array('eq' => $product->getId()))
                ->load(); 

            $order_ids = $collection->getColumnValues('order_id');

            foreach($order_ids as $order_id){
                $order = Mage::getModel('sales/order')->load($order_id);

                $productarray[] = array(
                    'id' => $order->getId(),
                    'product_id' => $product->getId(),
                    'name' => $product->getName(),
                    'customer_id' => $order->getCustomerId(),
                    'customer_name' => $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname(),
                    'customer_email' => $order->getCustomerEmail(),
                    'phone' => $order->getShippingAddress()->getTelephone(),
                    'booking_date' => $order->getCreatedAt(),
                    'qty' => $order->getTotalQtyOrdered(),
                    'status' => $order->getStatus(),
                    'sub_total' => $order->getSubtotal(),
                    'shipping_cost' => $order->getBaseShippingAmount(),
                    'voucher' => $order->getDiscountAmount(),
                    'base_grand_total' => $order->getBaseGrandTotal(),
                    'grand_total' => $order->getGrandTotal()
                ); 
            }
        }
        return $productarray;
    }

    public function _deleteBooking($data){
        $bookingId = $data;
        $order = Mage::getResourceModel('sales/order_collection')
                ->addFieldToSelect('*')
                ->addFieldToFilter('entity_id', $data)
                ->setOrder('created_at', 'desc'); 

        $order->delete();
        $message_dialog = array('message_dialog' => '200 Booking delete successfull');
        return $message_dialog;
    }

    public function _createBooking(){
        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();

        //Create sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getStoreId());

        $params = $this->getRequest()->getParams();
        $customer = Mage::getSingleton('customer/session')->getCustomer();

        // $customerEmail = $params['email'];
        // $customerFirstname = $params['name'];
        // $customerLastname =  $params['name'];

        //$customerEmail = $customer->getEmail();
        $customerFirstname = $customer->getFirstname();
        $customerLastname =  $customer->getLastname();
        $customerPhone =  $params['phone'];
        $customerNote =  $params['note'];
        $productQty =  $params['qty'];
        $productid =  $params['product'];
        $bookdate =  $params['bookdate'];
        $information = $params['guest'];
            
        $guest_info = json_encode($information, true);
        $billingAddress = array(
                        //'customer_address_id' => '',
                        'prefix' => '',
                        'firstname' => $customerFirstname,
                        'middlename' => '',
                        'lastname' => $customerLastname,
                        'suffix' => '',
                        'company' => '', 
                        'street' => array(
                            '0' => 'No Address', // compulsory
                            
                        ),
                        'city' => 'No City',
                        'country_id' => 'ID', // two letters country code
                        'region' => 'Indonesia', // can be empty '' if no region
                        'region_id' => 'ID', // can be empty '' if no region_id
                        'postcode' => '0000',
                        'telephone' => $customerPhone,
                        'fax' => '',
                        //'save_in_address_book' => 1
                    );

        $shippingMethod = 'flatrate_flatrate';
        $paymentMethod = 'snap';
            
        // Initialize sales quote object
        $quote = Mage::getModel('sales/quote')->setStoreId($store->getId());
            
        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode());
            
            //Check whether the customer already registered or not
            //$customer = Mage::getModel('customer/customer')->setWebsiteId($website->getId())->loadByEmail($customerEmail);

            // if (!$customer->getId()) {

            //     //Create the new customer account if not registered
            //     $customer = Mage::getModel('customer/customer'); 
            //     $customer->setWebsiteId($website->getId())
            //             ->setStore($store)
            //             ->setFirstname($customerFirstname)
            //             ->setLastname($customerLastname)
            //             ->setEmail($customerEmail);

            //     try {
            //         //$password = $customer->generatePassword(); 
            //         $customer->setPassword($customerPhone);

            //         //Set the customer as confirmed
            //         $customer->setForceConfirmed(true);
            //         $customer->save();

            //         $customer->setConfirmation(true);
            //         $customer->save();

        
            //         //Send new account email to customer
            //         $storeId = $customer->getSendemailStoreId();
            //         $customer->sendNewAccountEmail();

            //         //Set password remainder email if the password is auto generated by magento
            //         //$customer->sendPasswordReminderEmail();

            //     } catch (Exception $e) {
            //         Mage::logException($e);
            //     } 
            // }
      
            // Assign customer to quote
            $quote->assignCustomer($customer);

            //Add products to quote
            $product = Mage::getModel('catalog/product')->load($productid);
            $quote->addProduct($product, $productQty);
            
            // Add billing address to quote
            $billingAddressData = $quote->getBillingAddress()->addData($billingAddress);
            
            // Add shipping address to quote
            $shippingAddressData = $quote->getShippingAddress()->addData($billingAddress);
            
            // Collect shipping rates on quote shipping address data
            $shippingAddressData->setCollectShippingRates(true)
                                ->collectShippingRates();
            
            // Set shipping and payment method on quote shipping address data
            $shippingAddressData->setShippingMethod($shippingMethod)
                                ->setPaymentMethod($paymentMethod);
            
            // Set payment method for the quote
            $quote->getPayment()->importData(array('method' => $paymentMethod));
            
            try {
                // Collect totals of the quote
                $quote->collectTotals();
                $quote->setBookdate($bookdate);
                $quote->setInfoguest($guest_info);
                // Save quote
                $quote->save();
                
                // Create Order From Quote
                $service = Mage::getModel('sales/service_quote', $quote);
                $service->submitAll();
                $order = $service->getOrder();

                $this->sendNewOrderMail($customer, $order, $productid);
 
                $incrementId = $service->getOrder()->getRealOrderId();
                
                Mage::getSingleton('checkout/session')
                
                    ->setRealOrderId($incrementId)
                    ->setLastRealOrderId($incrementId);

                //->clearHelperData();
     
                // Log order created message
                //echo('Order created with increment id: '.$incrementId);
         
                $sampel = Mage::getSingleton('checkout/session')->getLastRealOrderId();

                //echo $sampel;
                //var_dump($sampel);     
                $result['success'] = true;
                $result['error']   = false;
                
                //$redirectUrl = Mage::getSingleton('checkout/session')->getRedirectUrl();
                //$redirectUrl = Mage::getUrl('checkout/onepage/success');
                //$result['redirect'] = $redirectUrl;

                $redirecturl = Mage::getBaseUrl().'snap/payment/redirect';
                
                // Show response
                $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
                $this->getResponse()->setRedirect($redirecturl);

                return $result;
                // $this->getResponse()->setBody(
                // Mage::helper('core')->jsonEncode($result));
        
            } catch (Mage_Core_Exception $e) {
                $result['success'] = false;
                $result['error'] = true;
                $result['error_messages'] = $e->getMessage();    
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                    Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                } else {
                    $messages = array_unique(explode("\n", $e->getMessage()));
                    foreach ($messages as $message) {
                        Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                    }
                }
            } catch (Exception $e) {
                $result['success']  = false;
                $result['error']    = true;
                $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                
                Mage::logException($e);
                //$this->_goBack();
            }
    }
//////////////////////////////////////////////////////////////////////////////////////////////////
    public function _getProductbyCustomerId($data){
        $customerId = $data;

        $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addFieldToFilter('status', '1')
            ->addAttributeToFilter('visibility', 4)
            //->addAttributeToFilter('attribute_set_id', '15')
            ->addFieldToFilter('customer_id', $customerId)
            ->addAttributeToSort('created_at', 'DESC')
            ->addAttributeToSelect('*')
            ->setPageSize(20)
            ->load();

        foreach ($products as $product) {
            $productarray[] = array(
                'id' => $product->getId()
            ); 
        }
        return $productarray;
/*
        foreach ($products as $product) {
            $order[] = Mage::getResourceModel('sales/order_collection')
                ->addFieldToSelect('*')
                ->addFieldToFilter('product_id', $product->getId())
                ->setOrder('created_at', 'desc'); 
        }
        return $order;*/
    }

    public function _checkAvailability(){
        //receive parameter from post
        $params = $this->getRequest()->getParams();
        $idprdk = $params['product'];
        $tgl = $params['bookdate'];

        //load order model by product id
        $ordermodel = Mage::getResourceModel('sales/order_item_collection')
        ->addAttributeToFilter('product_id', array('in', $idprdk))
        ->load();
        $order_ids = $ordermodel->getColumnValues('order_id');  
        

        //load product model by product id
        $product = Mage::getModel("catalog/product")->load($idprdk);
        $max_availability = $product->getTetapanqty();
       

        //check total order by product id
        foreach($order_ids as $order_id){
            $order = Mage::getModel('sales/order')->load($order_id);
            // count as order if status is:
            if ($order->getState() == 'new' || $order->getState() == 'processing' || $order->getState() == 'complete'){                
                //echo $order->getId();
                if($order->getBookdate() == $tgl ){                    
                    $qty[] = $order->getTotalQtyOrdered();                                        
                }                                
            }
        }
        
        $ordered_qty = array_sum($qty);
        $availability = $max_availability - $ordered_qty;
        return $availability;

        if( $availability >= 0){
            return $availability;
        }
        else{
            return 0;
        }
    }

    public function _testing1($data){
        $orderId = $data;
        $order = Mage::getModel("sales/order")->loadByIncrementId($orderId);

        $customerId = $order->getCustomerId();

        $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addFieldToFilter('status', '1')
            ->addAttributeToFilter('visibility', 4)
            //->addAttributeToFilter('attribute_set_id', '15')
            ->addFieldToFilter('customer_id', $customerId)
            ->addAttributeToSort('created_at', 'DESC')
            ->addAttributeToSelect('*')
            ->setPageSize(20)
            ->load();
            
        foreach ($products as $product) {
            $productarray[] = array(
                'id' => $order->getId(),
                'product_id' => $product->getId(),
                'name' => $product->getName(),
                'customer_id' => $order->getCustomerId(),
                'customer_name' => $order->getBillingAddress()->getFirstname().' '.$order->getBillingAddress()->getLastname(),
                'customer_email' => $order->getCustomerEmail(),
                'phone' => $order->getShippingAddress()->getTelephone(),
                'booking_date' => $order->getCreatedAt(),
                'qty' => $order->getTotalQtyOrdered(),
                'status' => $order->getStatus(),
                'sub_total' => $order->getSubtotal(),
                'shipping_cost' => $order->getBaseShippingAmount(),
                'voucher' => $order->getDiscountAmount(),
                'base_grand_total' => $order->getBaseGrandTotal(),
                'grand_total' => $order->getGrandTotal()
            ); 
        }
        return $productarray;
    }

    // public function _showBookingbyId($data){
    //     $bookingId = $data;
        
    //     $order = Mage::getResourceModel('sales/order_collection')
    //             ->addFieldToSelect('*')
    //             ->addFieldToFilter('entity_id', $bookingId)
    //             ->setOrder('created_at', 'desc'); 
    //     return $order;
    // }

    // public function _showBookingList($data){
    //     $customerId = $data;
    //     $order = Mage::getResourceModel('sales/order_collection')
    //             ->addFieldToSelect('*')
    //             ->addFieldToFilter('customer_id', $customerId)
    //             ->setOrder('created_at', 'desc');
    //     return $order;
    // }
}
?>