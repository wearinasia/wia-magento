<?php

class Rest_APIv1_CatalogController extends Mage_Core_Controller_Front_Action
{

    public function _sendAPI($data){
        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }

    public function _getSession(){
        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()){
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }
        else {
            $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
            return $message_dialog;
        }
    }

    public function IndexAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 0){
            //Get Parameter
            $params = $this->getRequest()->getParams();

            //Session
            $session = Mage::getSingleton('customer/session');

            if ($session->isLoggedIn()){
                $customer = $session->getId();
            }

            $getraws = $this->getRequest()->getRawBody();
            $rawbodys = json_decode($getraws, true);

            $product = $params['product'];
            $category = $params['category'];
            $location = $params['location'];
            $customerID = $params['customer_id'];
            $token = $rawbodys['token'];

            if ($_SERVER['REQUEST_METHOD'] === 'GET'){
                if(!empty($product)){
                    $print = $this->_showbyProductId($product);
                    $this->_sendAPI($print);
                }
                else if(!empty($category)){
                    $print = $this->_showbyCategoryId($category);
                    $this->_sendAPI($print);
                }
                else if(!empty($location)){
                    $print = $this->_showbyAttribute($location);
                    $this->_sendAPI($print);
                }   
                else if(!empty($customerID)){
                    $print = $this->_showbyCustomerId($customerID);
                    $this->_sendAPI($print);
                }
                else {
                    $print = $this->_showbyProductList();
                    $this->_sendAPI($print);
                }
            }
            else if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                //ADD -> EDIT DENGAN PRODUCT ID
                $new_productID = $this->_createProduct($customer);
                //$print = $this->_editProduct($new_productID);
                $this->_sendAPI($new_productID);
            }
            else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
                //EDIT PRODUCT ID BERDASARKAN CUSTOMER ID(CUSTOMER PUNYA LIST PRODUCT ID TSB)
                if ($token == $this->_getSession()){
                    $products = Mage::getModel("catalog/product")->load($product);
                    $productcomparasion = $products->getCustomerId();

                    if($productcomparasion == $customer){
                        $print = $this->_editProduct($product);
                        $this->_sendAPI($print);
                    }
                }
                else {
                    $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
                    $this->_sendAPI($message_dialog);
                }
            }
            else if ($_SERVER['REQUEST_METHOD'] === 'DELETE'){
                $print = $this->_deleteProduct($product);
                $this->_sendAPI($print);
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        }  
    }

    public function _showbyProductList(){
        
        $products = Mage::getModel("catalog/product")
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('status', '1')
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('attribute_set_id', '15')
                ->addAttributeToSort('created_at', 'DESC')
                ->setPageSize(20)
                ->load();
      
        foreach ($products as $product) {
            $productarray[] = array(
                'id' => $product->getId(),
                'url_key'=> $product->getUrlKey(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'description' => $product->getDescription(),
                'short_description' => $product->getShortDescription(),
                'customer_id' => $product->getCustomerId(),
                'exp_available_date' => Mage::helper('core')->jsonDecode($product->getExpAvailableDate()),
                'exp_duration' => $product->getExpDuration(),
                'exp_include' => Mage::helper('core')->jsonDecode($product->getExpInclude()),
                'exp_exclude' => Mage::helper('core')->jsonDecode($product->getExpExclude()),
                'exp_location' => $product->getExpLocation(),
                'exp_itenenary' => Mage::helper('core')->jsonDecode($product->getExpItenenary()),
                'tetapanqty' => $product->getTetapanqty(),
                //'media_gallery' => Mage::helper('catalog/image')->init($product, 'thumbnail')->resize(150,220)
                'media_gallery' => $product->getMediaGallery()
            );  
        }
        return $productarray;
    }

    public function _showbyProductId($data){
        $productId = $data;
        $product = Mage::getModel("catalog/product")->load($productId);

        $productarray[] = array(
                'id' => $product->getId(),
                'url_key'=> $product->getUrlKey(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'description' => $product->getDescription(),
                'short_description' => $product->getShortDescription(),
                'customer_id' => $product->getCustomerId(),
                'exp_available_date' => Mage::helper('core')->jsonDecode($product->getExpAvailableDate()),
                'exp_duration' => $product->getExpDuration(),
                'exp_include' => Mage::helper('core')->jsonDecode($product->getExpInclude()),
                'exp_exclude' => Mage::helper('core')->jsonDecode($product->getExpExclude()),
                'exp_location' => $product->getExpLocation(),
                'exp_itenenary' => Mage::helper('core')->jsonDecode($product->getExpItenenary()),
                'tetapanqty' => $product->getTetapanqty(),
                'media_gallery' => $product->getMediaGallery()
            ); 
        return $productarray;
    }

    public function _showbyCategoryId($data){
        $categoryId = $data;

        //Get Parameter
        $params = $this->getRequest()->getParams();

        $location = $params['location'];

        if(isset($location)){
            $products = Mage::getModel('catalog/category')->load($categoryId)
                            ->getProductCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('status', '1')
                            ->addAttributeToFilter('visibility', 4)
                            ->addAttributeToFilter('exp_location',$location)
                            //->addAttributeToFilter('attribute_set_id', '15')
                            ->addAttributeToSort('position')
                            ->setPageSize(20);
        }
        else {
            $products = Mage::getModel('catalog/category')->load($categoryId)
                            ->getProductCollection()
                            ->addAttributeToSelect('*')
                            ->addAttributeToFilter('status', '1')
                            ->addAttributeToFilter('visibility', 4)
                            //->addAttributeToFilter('attribute_set_id', '15')
                            ->addAttributeToSort('position')
                            ->setPageSize(20);
        }

        foreach ($products as $product) {
            $productarray[] = array(
                'id' => $product->getId(),
                'url_key'=> $product->getUrlKey(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'description' => $product->getDescription(),
                'short_description' => $product->getShortDescription(),
                'customer_id' => $product->getCustomerId(),
                'exp_available_date' => Mage::helper('core')->jsonDecode($product->getExpAvailableDate()),
                'exp_duration' => $product->getExpDuration(),
                'exp_include' => Mage::helper('core')->jsonDecode($product->getExpInclude()),
                'exp_exclude' => Mage::helper('core')->jsonDecode($product->getExpExclude()),
                'exp_location' => $product->getExpLocation(),
                'exp_itenenary' => Mage::helper('core')->jsonDecode($product->getExpItenenary()),
                'tetapanqty' => $product->getTetapanqty(),
                'media_gallery' => $product->getMediaGallery()
            );  
        }
        return $productarray;
    }

    public function _showbyAttribute($data){
        //MASI TESTING
        $location = $data;
        $products = Mage::getModel("catalog/product")
                ->getCollection()
                ->addFieldToFilter('status', '1')
                ->addAttributeToFilter('visibility', 4)
                ->addAttributeToFilter('attribute_set_id', '15')
                ->addAttributeToFilter('exp_location', $location)
                ->addAttributeToSort('created_at', 'DESC')
                ->addAttributeToSelect('*')
                ->setPageSize(20)
                ->load();

        foreach ($products as $product) {
            $productarray[] = array(
                'id' => $product->getId(),
                'url_key'=> $product->getUrlKey(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'description' => $product->getDescription(),
                'short_description' => $product->getShortDescription(),
                'customer_id' => $product->getCustomerId(),
                'exp_available_date' => Mage::helper('core')->jsonDecode($product->getExpAvailableDate()),
                'exp_duration' => $product->getExpDuration(),
                'exp_include' => Mage::helper('core')->jsonDecode($product->getExpInclude()),
                'exp_exclude' => Mage::helper('core')->jsonDecode($product->getExpExclude()),
                'exp_location' => $product->getExpLocation(),
                'exp_itenenary' => Mage::helper('core')->jsonDecode($product->getExpItenenary()),
                'tetapanqty' => $product->getTetapanqty(),
                'media_gallery' => $product->getMediaGallery()
            ); 
        }
        return $productarray;
    }

    public function _showbyCustomerId($data){
        //$customerId = $session->getId();
        $customerId = $data;

        $products = Mage::getModel("catalog/product")
            ->getCollection()
            ->addFieldToFilter('status', '1')
            ->addAttributeToFilter('visibility', 4)
            //->addAttributeToFilter('attribute_set_id', '15')
            ->addFieldToFilter('customer_id', $customerId)
            ->addAttributeToSort('created_at', 'DESC')
            ->addAttributeToSelect('*')
            ->setPageSize(20)
            ->load();

        foreach ($products as $product) {
            $productarray[] = array(
                'id' => $product->getId(),
                'url_key'=> $product->getUrlKey(),
                'name' => $product->getName(),
                'price' => $product->getPrice(),
                'description' => $product->getDescription(),
                'short_description' => $product->getShortDescription(),
                'customer_id' => $product->getCustomerId(),
                'exp_available_date' => Mage::helper('core')->jsonDecode($product->getExpAvailableDate()),
                'exp_duration' => $product->getExpDuration(),
                'exp_include' => Mage::helper('core')->jsonDecode($product->getExpInclude()),
                'exp_exclude' => Mage::helper('core')->jsonDecode($product->getExpExclude()),
                'exp_location' => $product->getExpLocation(),
                'exp_itenenary' => Mage::helper('core')->jsonDecode($product->getExpItenenary()),
                'tetapanqty' => $product->getTetapanqty(),
                'media_gallery' => $product->getMediaGallery()
            ); 
        }
        return $productarray;
    }

    public function _editProduct($data){
        $productId = $data;
        $product = Mage::getModel("catalog/product")->load($productId);
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $getraws = $this->getRequest()->getRawBody();
        $inputed_rawbodys = json_decode($getraws, true);

        $inputed_Name = $inputed_rawbodys['name'];
        $inputed_Price = $inputed_rawbodys['price'];
        $inputed_Description = $inputed_rawbodys['description'];
        $inputed_ShortDescription = $inputed_rawbodys['short_description'];
        $inputed_ExpAvailableDate = json_encode($array = array (Mage::getModel('core/date')->date('Y-m-d')));
        $inputed_ExpInclude = $inputed_rawbodys['exp_include'];
        $inputed_ExpExclude = $inputed_rawbodys['exp_exclude'];
        $inputed_ExpLocation = $inputed_rawbodys['exp_location'];
        $inputed_ExpDuration = $inputed_rawbodys['exp_duration'];
        $inputed_ExpItenenary = $inputed_rawbodys['exp_itenenary'];
        $inputed_TetapanQty = $inputed_rawbodys['tetapanqty'];

        $experience_id = $product->getId();
        $experience_url_key = $product->getUrlKey();
        $experience_name = $product->getName();
        $experience_price = $product->getPrice();
        $experience_description = $product->getDescription();
        $experience_short_description = $product->getShortDescription();
        $experience_customer_id = $product->getCustomerId();
        $experience_exp_available_date = Mage::helper('core')->jsonDecode($product->getExpAvailableDate());
        $experience_exp_duration = $product->getExpDuration();
        $experience_exp_include = Mage::helper('core')->jsonDecode($product->getExpInclude());
        $experience_exp_exclude = Mage::helper('core')->jsonDecode($product->getExpExclude());
        $experience_exp_location = $product->getExpLocation();
        $experience_exp_itenenary = Mage::helper('core')->jsonDecode($product->getExpItenenary());
        $experience_tetapanqty = $product->getTetapanqty();
        $experience_media_gallery = $product->getMediaGallery();

        if(!empty($inputed_Name) && $experience_name != $inputed_Name){
            $product->setName($inputed_Name);
            $product->save();
        }
        if(!empty($inputed_Price) && $experience_price != $inputed_Price){
            $product->setPrice($inputed_Price);
            $product->save();
        }
        if(!empty($inputed_Description) && $experience_description != $inputed_Description){
            $product->setDescription($inputed_Description);
            $product->save();
        }
        if(!empty($inputed_ShortDescription) && $experience_short_description != $inputed_ShortDescription){
            $product->setShortDescription($inputed_ShortDescription);
            $product->save();
        }
        if(!empty($inputed_ExpAvailableDate) && $experience_exp_available_date != $inputed_ExpAvailableDate){
            $product->setExpAvailableDate($inputed_ExpAvailableDate);
            $product->save();
        }
        if(!empty($inputed_ExpInclude) && $experience_exp_include != $inputed_ExpInclude){
            $product->setExpInclude(json_encode($inputed_ExpInclude));
            $product->save();
        }
        if(!empty($inputed_ExpExclude) && $experience_exp_exclude != $inputed_ExpExclude){
            $product->setExpExclude(json_encode($inputed_ExpExclude));
            $product->save();
        }
        if(!empty($inputed_ExpLocation) && $experience_exp_location != $inputed_ExpLocation){
            $product->setExpLocation($inputed_ExpLocation);
            $product->save();
        }
        if(!empty($inputed_ExpDuration) && $experience_exp_duration != $inputed_ExpDuration){
            $product->setExpDuration($inputed_ExpDuration);
            $product->save();
        }
        if(!empty($inputed_ExpItenenary) && $experience_exp_itenenary != $inputed_ExpItenenary){
            $product->setExpItenenary(json_encode($inputed_ExpItenenary));
            $product->save();
        }
        if(!empty($inputed_TetapanQty) && $experience_tetapanqty != $inputed_TetapanQty){
            $product->setTetapanqty($inputed_TetapanQty);
            $product->save();
        }
        else {}
        return $product;   
    }

    public function _createProduct($data){
        $customerId = $data;
        
        try{
            $product = Mage::getModel("catalog/product");
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

            $product
            ->setStoreId(0) 
            ->setWebsiteIds(array(1)) 
            ->setAttributeSetId(15) 
            ->setTypeId('simple') 
            ->setCreatedAt(strtotime('now')) 
            ->setSku($customerId.'-'.strtotime('now'))
            ->setWeight(0)
            ->setStatus(1)
            ->setTaxClassId(0)
            ->setVisibility(1)         
            
            ->setCustomerId($customerId)
            ->setCategoryIds(array(null))
            ->setCustomDesign('wia-new/experience')
            ->setStockData(array('use_config_manage_stock' => 0,'manage_stock'=>0)); 

            $product->save();//echo "1";
            $new_product = $product->getId();
        }
       
        catch(Exception $e){
            
        }

    //$this->sendNewOrderMail($new_product);
    $message_code = array('id' => $new_product);
    return $message_code;
    }

    public function _deleteProduct($data){
        $productId = $data;
        $product = Mage::getModel("catalog/product")->load($productId);

        $product->delete();
        $message_dialog = array('message_dialog' => '200 Product delete successful');
        return $message_dialog;
    }

    public function sendNewOrderMail($new_product){
           
            // $new_product = 4082;
            // echo $new_product;

            $mail = Mage::getModel('core/email');
            $mail->setToName('Leon');
            $mail->setToEmail('leon.christopher@student.umn.ac.id');
            
            $url= 'https://wia.id/index.php/dapur/catalog_product/edit/id/'. $new_product;
            $button = '<a href="'.$url.'">Review Experience</a>';
            
            $mail->setSubject('Experience baru');
            $mail->setBody('Ada penambahan experience baru, mohon untuk mereview</br></br>'.$button);
            $mail->setFromEmail('contact@wearinasia.com');
            $mail->setFromName('Wearinasia');
            $mail->setType('html');

            try {
                $mail->send();
                Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
                return true;
            } catch (Exception $e) {
                Mage::getSingleton('core/session')->addError('Unable to send.');
                return $e->getMessage();
            }
    }

    public function TestingAction(){
        //Get Parameter
        $params = $this->getRequest()->getParams();

        //Session
        $session = Mage::getSingleton('customer/session');

        if ($session->isLoggedIn()){
            $customer = $session->getId();
        }

        $getraws = $this->getRequest()->getRawBody();
        $rawbodys = json_decode($getraws, true);

        $product = $params['product'];
        $category = $params['category'];
        $location = $params['location'];
        $customerID = $params['customer_id'];
        $token = $rawbodys['token'];

        if ($_SERVER['REQUEST_METHOD'] === 'GET'){
            if(!empty($product)){
                $print = $this->_showbyProductId($product);
                $this->_sendAPI($print);
            }
            else if(!empty($category)){
                $print = $this->_showbyCategoryId($category);
                $this->_sendAPI($print);
            }
            else if(!empty($location)){
                $print = $this->_showbyAttribute($location);
                $this->_sendAPI($print);
            }
            else if(!empty($customerID)){
                $print = $this->_showbyCustomerId($customerID);
                $this->_sendAPI($print);
            }
            else {
                $print = $this->_showbyProductList();
                $this->_sendAPI($print);
            }
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'POST'){
            //ADD -> EDIT DENGAN PRODUCT ID
            $new_productID = $this->_createProduct($customer);
            $print = $this->_editProduct($new_productID);
            $this->_sendAPI($print);
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
            //EDIT PRODUCT ID BERDASARKAN CUSTOMER ID(CUSTOMER PUNYA LIST PRODUCT ID TSB)
            if ($token == $this->_getSession()){
                $products = Mage::getModel("catalog/product")->load($product);
                $productcomparasion = $products->getCustomerId();

                if($productcomparasion == $customer){
                    $print = $this->_editProduct($product);
                    $this->_sendAPI($print);
                }
            }
            else {
                $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
                $this->_sendAPI($message_dialog);
            }
        }
        else if ($_SERVER['REQUEST_METHOD'] === 'DELETE'){
            $print = $this->_deleteProduct($product);
            $this->_sendAPI($print);
        }
        else {
            $message_code = array('message_code' => '405 Method Not Allowed');
            $this->_sendAPI($message_code);
        }
    }

    //Location
    //Language
    //Category
    //Your Background
    //Experience Page
    //Settings
    //Review & Submit
    //Confirm ID
}
?>