<?php
 
class Rest_APIv1_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        return Mage::helper('Rest_APIv1_helper')->helloHelper();
    }
 
    public function sayHelloAction()
    {
        echo 'Hello one more time...';
    }
}
?>