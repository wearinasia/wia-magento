<?php

class Rest_APIv1_PromotionController extends Mage_Core_Controller_Front_Action
{

    //Library Message Codes
    //101 - Headers error - Wrong Source or Authorization value
    //102 - Error - No Source or Authorization value
    //103 - Wrong Request Method

    public function _sendAPI($data){

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
        
    }

    public function _validation(){

        //Header source app
        $source = Mage::helper('Rest_APIv1_helper')->HeaderValidation();

        //Header Authorization
        $auth = Mage::helper('Rest_APIv1_helper')->AuthenticationValidation();

        if(isset($source) && isset($auth)){
            if(!empty($source) && $auth == "Bearer AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc"){
                return 1;
            }
            else {
                $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
                $this->_sendAPI($message_code);
                return 0;
            }
        }
        else {
            $message_code = array('message_code' => '400 Error - No Source or Authorization value');
            $this->_sendAPI($message_code);
            return 0;
        }
    }

    public function IndexAction(){

        $validate = $this->_validation();

        if($validate == 1){
            //Get Parameter
            $params = $this->getRequest()->getParams();
        }  
    }

}
?>