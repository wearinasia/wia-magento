<?php
 
class Rest_APIv1_User_BookingController extends Mage_Core_Controller_Front_Action
{
	public function _sendAPI($data){

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }

    public function _getSession(){
        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()){
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }
        else {
            $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
            return $message_dialog;
        }
    }
    
	public function BookHistoryAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 1){
            if ($_SERVER['REQUEST_METHOD'] === 'GET'){
                //Session
                $session = Mage::getSingleton('customer/session');
                if ($session->isLoggedIn()) {
                    $customerId = $session->getId();
                    $order = Mage::getModel("sales/order")->load($customerId);

                    foreach ($order as $order) {
                        $result[]= $order->getData();
                    }

                    $this->_sendAPI($result);            
                }
                else {
                    $customerId = $this->_getSession();                                       
                    $this->_sendAPI($customerId);
                }  
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
    }

    public function BankAccountAction(){
    }

    public function PayoutRequestAction(){

    }
}