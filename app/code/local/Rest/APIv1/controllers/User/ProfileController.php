<?php
 
class Rest_APIv1_User_ProfileController extends Mage_Core_Controller_Front_Action
{

    //Library Message Codes
    //101 - Headers error - Wrong Source or Authorization value
    //102 - Error - No Source or Authorization value
    //103 - Wrong Request Method

    public function _sendAPI($data){

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }

    public function _getSession(){
        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()){
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }
        else {
            $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
            return $message_dialog;
        }
    }

    public function IndexAction(){

        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 1){
            if ($_SERVER['REQUEST_METHOD'] === 'GET'){
                //$customerToken = $params['token'];
                //Session
                $session = Mage::getSingleton('customer/session');
                //$customersession = $this->_getSession();
                        
                if ($session->isLoggedIn()) {
                    $customerId = $session->getId();

                    $customer = Mage::getModel('customer/customer')->load($customerId);
                    $this->_sendAPI($customer);
                }
                else {
                    $customerId = $this->_getSession();                                       
                    $this->_sendAPI($customerId);
                }  
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

    public function EditCustomerAction(){
        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 1){
            if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
                //Session
                $session = Mage::getSingleton('customer/session');

                if ($session->isLoggedIn()) {
                    $customerId = $session->getId();
                    $customer = Mage::getModel('customer/customer')->load($customerId);
                        
                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);

                    $customerFirstname = $params['firstname'];
                    $customerLastname = $params['lastname'];
                    $customerTitle = $params['title'];
                    $customerBio = $params['bio'];
                    $customerProfileImage = $params['photo_url'];
                    $customerEmail = $params['email'];
                    $customerPhone = $params['phone'];

                    $firstname = $customer->getFirstname();
                    $lastname = $customer->getLastname();
                    $title = $customer->getData('title');
                    $bio = $customer->getData('bio');
                    $profileimage = $customer->getData('photo_url');
                    $email = $customer->getEmail();
                    $phone = $customer->getData('phone');

                    if(!empty($customerFirstname) && $firstname != $customerFirstname){
                        $customer->setFirstname($customerFirstname);
                        $customer->save();
                    }
                    if(!empty($customerLastname) && $lastname != $customerLastname){
                        $customer->setLastname($customerLastname);
                        $customer->save();
                    }
                    if(!empty($customerTitle) && $title != $customerTitle){
                        $customer->setTitle($customerTitle);
                        $customer->save();
                    }
                    if(!empty($customerBio) && $bio != $customerBio){
                        $customer->setBio($customerBio);
                        $customer->save();
                    }
                    if(!empty($customerProfileImage) && $profileimage != $customerProfileImage){
                        $customer->setFirstname($customerProfileImage);
                        $customer->save();
                    }    
                    if(!empty($customerEmail) && $email != $customerEmail){
                        $customer->setEmail($customerEmail);
                        $customer->save();
                    }
                    if(!empty($customerPhone) && $phone != $customerPhone){
                        $customer->setPhone($customerPhone);
                        $customer->save();
                    }
                    else {}
                    $this->_sendAPI($customer);   
                }
                else {
                    $customerId = $this->_getSession();                                       
                    $this->_sendAPI($customerId);
                }
            }
            else {
                $message_code = array('message_code' => '405 Method Not Allowed');
                $this->_sendAPI($message_code);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

    public function BankAction(){
        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 1){
            $session = Mage::getSingleton('customer/session');
            if ($session->isLoggedIn()) {
                if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                    $customerId = $session->getId();
                    $modelaccount = Mage::getModel('exp/account')->load($customerId);
                    //$this->_sendAPI($modelaccount);

                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);

                    $namaakun = $params['acc_name'];
                    $nomorakun = $params['acc_number'];
                    $namabank = $params['bank_name'];
                    $modelaccount->setAccName($namaakun);
                    $modelaccount->setAccNumber($nomorakun);
                    $modelaccount->setBankName($namabank);
                    $modelaccount->save();

                    $this->_sendAPI($modelaccount);  
                }
                else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
                    $customerId = $session->getId();
                    $modelaccount = Mage::getModel('exp/account')->load($customerId);
                    //$this->_sendAPI($modelaccount);

                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);

                    $namaakun = $params['acc_name'];
                    $nomorakun = $params['acc_number'];
                    $namabank = $params['bank_name'];

                    $accname = $modelaccount->getAccName();
                    $accnumber = $modelaccount->getAccNumber();
                    $bankname = $modelaccount->getBankName();

                    if(!empty($namaakun && $accname != $namaakun)){
                        $modelaccount->setAccName($namaakun);
                        $modelaccount->save();
                    }
                    if(!empty($nomorakun && $accnumber != $nomorakun)){
                        $modelaccount->setAccNumber($nomorakun);
                        $modelaccount->save();
                    }
                    if(!empty($namabank)){
                        $modelaccount->setBankName($namabank);
                        $modelaccount->save();
                    }

                    $this->_sendAPI($modelaccount);
                }
                else {
                    $message_code = array('message_code' => '405 Method Not Allowed');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $customerId = $this->_getSession();                                       
                $this->_sendAPI($customerId);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

    public function AddressAction(){
        //Validation
        $validate = Mage::helper('Rest_APIv1_helper')->AuthHeaderValidation();

        if($validate == 1){
            $session = Mage::getSingleton('customer/session');
            if ($session->isLoggedIn()) {
                if ($_SERVER['REQUEST_METHOD'] === 'POST'){
                    $customerId = $session->getId();
                    $customer = Mage::getModel('customer/customer')->load($customerId);
                    $address = $customer->getPrimaryShippingAddress();

                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);

                    $customerCountry = $params['country'];
                    $customerPostalcode = $params['postal_code'];
                    $customerCity = $params['city'];
                    $customerTelephone = $params['phone'];
                    $customerFax = $params['fax'];
                    $customerCompany = $params['company'];
                    $customerStreet = $params['street']; 

                    $address = Mage::getModel("customer/address");
                    $address->setCustomerId($customer->getId());
                    $address->setFirstname($customer->getFirstname());
                    $address->setLastname($customer->getLastname());
                    $address->setCountryId($customerCountry);
                    $address->setPostcode($customerPostalcode);
                    $address->setCity($customerCity);
                    $address->setTelephone($customerTelephone);
                    $address->setFax($customerFax);
                    $address->setCompany($customerCompany);
                    $address->setStreet($customerStreet);
                    $address->setIsDefaultBilling('1');
                    $address->setIsDefaultShipping('1');
                    $address->setSaveInAddressBook('1');

                    $address->save();

                    $this->_sendAPI($address);
                }
                else if ($_SERVER['REQUEST_METHOD'] === 'PUT'){
                    $customerId = $session->getId();
                    $customer = Mage::getModel('customer/customer')->load($customerId);
                    $address = $customer->getPrimaryShippingAddress();

                    $getraws = $this->getRequest()->getRawBody();
                    $params = json_decode($getraws, true);

                    $firstname = $address->getFirstname();
                    $lastname = $address->getLastname();
                    $country = $address->getCountryId();
                    $postalcode = $address->getPostcode();
                    $city = $address->getCity();
                    $telephone = $address->getTelephone();
                    $fax = $address->getFax();
                    $company = $address->getCompany();
                    $street = $address->getStreet();

                    $customerFirstname = $params['firstname'];
                    $customerLastname = $params['lastname'];
                    $customerCountry = $params['country'];
                    $customerPostalcode = $params['postal_code'];
                    $customerCity = $params['city'];
                    $customerTelephone = $params['phone'];
                    $customerFax = $params['fax'];
                    $customerCompany = $params['company'];
                    $customerStreet = $params['street']; 

                    if(!empty($customerFirstname) && $firstname != $customerFirstname){
                        $address->setFirstname($customerFirstname);
                        $address->save();
                    }
                    if(!empty($customerLastname) && $lastname != $customerLastname){
                        $address->setLastname($customerLastname);
                        $address->save();
                    }
                    if(!empty($customerCountry) && $country != $customerCountry){
                        $address->setCountryId($customerCountry);
                        $address->save();
                    }
                    if(!empty($customerPostalcode) && $postalcode != $customerPostalcode){
                        $address->setPostcode($customerPostalcode);
                        $address->save();
                    }
                    if(!empty($customerCity) && $city != $customerCity){
                        $address->setCity($customerCity);
                        $address->save();
                    }    
                    if(!empty($customerTelephone) && $telephone != $customerTelephone){
                        $address->setTelephone($customerTelephone);
                        $address->save();
                    }
                    if(!empty($customerFax) && $fax != $customerFax){
                        $address->setFax($customerFax);
                        $address->save();
                    }
                    if(!empty($customerCompany) && $company != $customerCompany){
                        $address->setCompany($customerCompany);
                        $address->save();
                    }
                    if(!empty($customerStreet) && $street != $customerStreet){
                        $address->setStreet($customerStreet);
                        $address->save();
                    }
                    else {}
                    $this->_sendAPI($address);   
                }
                else {
                    $message_code = array('message_code' => '405 Method Not Allowed');
                    $this->_sendAPI($message_code);
                }
            }
            else {
                $customerId = $this->_getSession();                                       
                $this->_sendAPI($customerId);
            }
        }
        else {
            $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
            $this->_sendAPI($message_code);
        } 
    }

}
?>