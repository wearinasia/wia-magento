<?php
 
class Rest_APIv1_UserController extends Mage_Core_Controller_Front_Action
{

    //Library Message Codes
    //101 - Headers error - Wrong Source or Authorization value
    //102 - Error - No Source or Authorization value
    //103 - Wrong Request Method

    public function _sendAPI($data){

        return $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
    }

    public function _validation(){

        //Header source app
        $source = Mage::helper('Rest_APIv1_helper')->HeaderValidation();

        //Header Authorization
        $auth = Mage::helper('Rest_APIv1_helper')->AuthenticationValidation();

        if(isset($source) && isset($auth)){
            if(!empty($source) && $auth == "Bearer AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc"){
                return 1;
            }
            else {
                $message_code = array('message_code' => '406 Headers error - Wrong Source or Authorization value');
                $this->_sendAPI($message_code);
                return 0;
            }
        }
        else {
            $message_code = array('message_code' => '400 Error - No Source or Authorization value');
            $this->_sendAPI($message_code);
            return 0;
        }
    }

    public function _getSession(){
        $session = Mage::getSingleton('customer/session');

        if($session->isLoggedIn()){
            $SID = $session->getEncryptedSessionId();
            return $SID;
        }
        else {
            $message_dialog = array('message_dialog' => '401 Your session has expired due to inactivity. Please Login again.');
            return $message_dialog;
        }
    }

    public function IndexAction(){
        $collections = Mage::getModel('customer/customer')->getCollection();
        foreach ($collections as $collection) {
            $result[]= $collection->getData();
        }

        $this->_sendAPI($result);
    }

}
?>