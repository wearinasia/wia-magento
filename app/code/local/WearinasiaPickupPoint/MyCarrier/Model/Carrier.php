<?php

class WearinasiaPickupPoint_MyCarrier_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {


    protected $_code = 'wearinasiapickuppoint_mycarrier';
    const IND_COUNTRY_ID = 'IDN';
    protected $rateResultFactory;
    protected $rateMethodFactory;

    public function __construct
    (
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        array $data = []
    )
    {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    public function collectRates(Mage_Shipping_Model_Rate_Request $request) 
    {
        $result = Mage::getModel('shipping/rate_result');
       $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
       
       $result->append($this->_getFreeOneDayShippingRate());



        return $result;
        
    }



    protected function _getFreeOneDayShippingRate() {
        $rate = Mage::getModel('shipping/rate_result_method');
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */

        $rate->setCarrier($this->_code);
        /**
         * getConfigData(config_key) returns the configuration value for the
         * carriers/[carrier_code]/[config_key]
         * Jabodetabek, rp 250rb min
         */
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('freeoneday');
        $rate->setMethodTitle('Wearinasia Experience Store, Gading Serpong');
        $rate->setMethodDescription('Pengambilan langsung pada hari dan jam operasional');
        $rate->setPrice($this->getConfigData('priceFreeOneDay'));
        $rate->setCost($this->getConfigData('priceFreeOneDay'));
        return $rate;
    }
	
    
/*
    protected function _getFreeShippingRate()
    {
        $rate = Mage::getModel('shipping/rate_result_method');
        
        $rate->setCarrier($this->_code);
        $rate->setCarrierTitle($this->getConfigData('title'));
        $rate->setMethod('free_shipping');
        $rate->setMethodTitle('Free Shipping');      
        $rate->setPrice($this->getConfigData('priceFree'));
        $rate->setCost(0);
        return $rate;       
    }*/
    
    public function getAllowedMethods() {
        return array(
            'freeoneday' => 'Free 1 Day Shipping',
           
        );
    }

/*
    public function checkShipCountry()
    {
        $speCountry = $this->getConfigData('sallowspecific');
        if($speCountry && $speCountry == 1)
        {
            $showMethod = $this->getConfigData('showmethod');
            $availableCountry = array();
            if($this->getConfigData('speCountry'))
            {
                $availableCountry = explode(',', $this->getConfigData('speCountry'));     
            }
            if()
        }
    }*/



}
