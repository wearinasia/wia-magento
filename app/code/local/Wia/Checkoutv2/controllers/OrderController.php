<?php
    class Wia_Checkoutv2_OrderController extends Mage_Core_Controller_Front_Action
    {

        public function indexAction(){
            $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
			$totalWeight = 0;
			foreach($items as $item) {
				$totalWeight += ($item->getWeight() * $item->getQty()) ;
			}
			if($totalWeight < 1)
			$totalWeight = 1;
			echo $totalWeight * 1000;

        }
        
        protected function _getCart()
        {
            return Mage::getSingleton('checkout/cart');
        }

        protected function _getSession()
        {
            return Mage::getSingleton('checkout/session');
        }

    
        protected function _getQuote()
        {
            return $this->_getCart()->getQuote();
        }

        protected function _clearsession(){
            // Clear Magento cart and quote items
            $cart = Mage::getModel('checkout/cart');                
            $cart->truncate()->save(); // remove all active items in cart page
            $cart->init();
            $session= Mage::getSingleton('checkout/session');
            $quote = $session->getQuote();
            $cart = Mage::getModel('checkout/cart');
            $cartItems = $cart->getItems();
            foreach ($cartItems as $item)
            {
                $quote->removeItem($item->getId())->save();
            }
            Mage::getSingleton('checkout/session')->clear();
                        
        }

       
   
    //Get shipping address
    public function getShippingAddressAjaxAction(){

        $this->updateShippingAddress();
        
    }



    //Add new shipping addresss for old customer
    public function addAddressAction(){
            
        if (!$this->_validateFormKey()) {
            $this->_redirect('*/*');
            return;
        }

            //login customer data
            $websiteId = Mage::app()->getWebsite()->getId();
            $store = Mage::app()->getStore();
            $customer = Mage::getSingleton('customer/session')->getCustomer();
            
        
            $params = $this->getRequest()->getParams();
            //var_dump($params);
            $firstName = $customer->getFirstname();
            $lastName = $customer->getLastname();
            $region = $params['region_id'];
            $code = $params['postcode'];
            $city = $params['city'];
            $street = $params['street'];
            $location = $params['location'];
            $phone = $params['telephone'];
            $vat = $params['vat_id'];




            $shippingAddress = array(
            'firstname' => $firstName,
            'middlename' => '',
            'lastname' => $lastName,
            'street' => array(
                '0' => $street, // compulsory
                '1' => $location // optional
            ),
            'city' => $city,
            'country_id' => 'ID', // two letters country code
            'region' => 'ds', // can be empty '' if no region
            'region_id' => $region, // can be empty '' if no region_id
            'postcode' => $code,
            'telephone' => $phone,
            'fax' => '',
            'vat_id' => $vat,
            'save_in_address_book' => 1
        );


        $Address = Mage::getModel('customer/address');  
        $Address->setData($shippingAddress)
                                ->setCustomerId($customer->getId())
                                ->setIsDefaultBilling('1')
                                ->setIsDefaultShipping('1')
                                ->setSaveInAddressBook('1');
        $Address->save();

       $this->_redirect('checkout/onepage');
       
      
       

       
       
    }

    public function setShippingAddressAction(){
        // $params = $this->getRequest()->getParams();
        // $code = $params['estimate_method'];
        
            
        $code = $this->getRequest()->getParam('estimate_method'); 
       
        $customer= Mage::getSingleton('customer/session')->getCustomer();
        $customerShippingAddress = $customer->getPrimaryShippingAddress();


        // Update the cart's quote.
        $cart = Mage::getSingleton('checkout/cart');

        $address = $cart->getQuote()->getShippingAddress();
        $address
                ->setShippingMethod($code)
                ->setCollectShippingrates(true);
        $cart->save();
       
        //update ordersummary
        $this->updateOrderSummaryAction();
       

    }



    

    public function updateOrderSummaryAction()
    {
        $layout = $this->getLayout();
        $shippingaddressBlock = $layout->createBlock('checkout/cart_totals')->setTemplate('checkout/cart/totals.phtml');
        echo $shippingaddressBlock->toHtml();

        
    

        
    }
    
      //update shipping address response
    public function updateShippingMethodAction(){
        $layout = $this->getLayout();
        $shippingaddressBlock = $layout->createBlock('checkout/cart_totals')->setTemplate('operationalmodule/shipping-method.phtml');
        echo $shippingaddressBlock->toHtml();

        

           
        
    }
    

    //update shipping address response
    public function updateShippingAddressAction(){

        $layout = $this->getLayout();
        $shippingaddressBlock = $layout->createBlock('checkout/cart_totals')->setTemplate('operationalmodule/shipping-address.phtml');
        echo $shippingaddressBlock->toHtml();

        

           
        
    }

   
    

   



    //apply voucher code

    public function applyVoucherAction(){
       
        $code = $this->getRequest()->getParam('code');
        Mage::getSingleton('checkout/cart')->getQuote()
        ->setCouponCode($code)->collectTotals()->save();
        $this->updateOrderSummaryAction();

        
        $rule =  Mage::getSingleton('checkout/session')->getQuote()->getAppliedRuleIds();
        if( $code == 'remove' ){
           
            echo '<div class="callout alert"><p>Promo Code Remove!</p></div>';
        }
        elseif( $rule == 0){
            echo '<div class="callout alert"><p>Promo Code Not Valid!</p></div>';
        }
        else{
            echo '<div class="callout success"><p>Promo Code Applied!</p></div>';
        }
       
        

    }

    public function createOrderAction(){
        
        if (!$this->_validateFormKey()) {
            $this->_redirect('/success');
            return;
        }

        $store = Mage::app()->getStore();
        $website = Mage::app()->getWebsite();
        $customer= Mage::getSingleton('customer/session')->getCustomer();
        
        $formKeys = Mage::getSingleton('core/session')->getFormKey();

        $shippingMethod =  Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod();
        
        $paymentMethod = 'snapio';

         // Initialize sales quote object
         $quote = Mage::getModel('sales/quote')
         ->setStoreId($store->getId());


        $checkout_session = Mage::getSingleton('checkout/session');
        //$cq = $checkout_session->getQuote();
        //$cq->assignCustomer(Mheage::getSingleton('customer/session')->getCustomer());
        $quote = Mage::getModel('sales/quote')->load($checkout_session->getQuoteId()); // Mage_Sales_Model_Quote
        $quote->getAllItems();                  
        //$quote->reserveOrderId();
        // Set currency for the quote
        $quote->setCurrency(Mage::app()->getStore()->getBaseCurrencyCode()); 

        // Assign customer to quote
        $quote->assignCustomer($customer);

        $billingAddressData = $quote->getBillingAddress();//->addData($customerBillingAddress);
        $shippingAddressData = $quote->getShippingAddress();//->addData($customerShippingAddress);

         // Set shipping and payment method on quote shipping address data
         $shippingAddressData->setShippingMethod($shippingMethod)
         ->setPaymentMethod($paymentMethod);

            // Set payment method for the quote
        $quote->getPayment()->importData(array('method' => $paymentMethod));


            try {
                // Collect totals of the quote
                $quote->collectTotals();
             
                // Save quote
                $quote->save();
                
                // Create Order From Quote
                $service = Mage::getModel('sales/service_quote', $quote);
                $service->submitAll();
                $incrementId = $service->getOrder()->getRealOrderId();
                $orderId = $service->getOrder()->getId();
                
 
                Mage::getSingleton('checkout/session')
                //    ->setQuoteId('123')
                //    ->setLastRealOrderId('123');
                    ->setRealOrderId($incrementId)
                    ->setLastRealOrderId($incrementId);
 
 
                 //set comment
                 $order = Mage::getModel('sales/order')->load($orderId);
                 $order->sendNewOrderEmail();
                 //$order->addStatusHistoryComment('Booking Date : '.$date.'</br>'.'Booking Time '.$time);
                 $order->save();
 
 
 
                //->clearHelperData();
               
                /**
                 * For more details about saving order
                 * See saveOrder() function of app/code/core/Mage/Checkout/Onepage.php
                 */ 
                
                //$result['redirect'] = 'hello';
             $result['success'] = true;
             $result['error'] = false;
             
             $redirecturl = Mage::getBaseUrl().'snapio/payment/redirect';
                 
             
             //$response['response'] = $this->myAjax();
             $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json',true);
             $this->getResponse()->setRedirect($redirecturl);
             $this->getResponse()->setBody(
             Mage::helper('core')->jsonEncode($result));
 
                
                 
             } catch (Mage_Core_Exception $e) {
                 //$redirecturl = Mage::getBaseUrl().'snapbarugopay/payment/redirect';
                 $result['success'] = false;
                 $result['error'] = true;
                 $result['error_messages'] = $e->getMessage();    
                 Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                 
                 if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                     Mage::getSingleton('checkout/session')->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
                 } else {
                     $messages = array_unique(explode("\n", $e->getMessage()));
                     foreach ($messages as $message) {
                         Mage::getSingleton('checkout/session')->addError(Mage::helper('core')->escapeHtml($message));
                     }
                 }
             } catch (Exception $e) {
                 $result['success']  = false;
                 $result['error']    = true;
                 $result['error_messages'] = $this->__('There was an error processing your order. Please contact us or try again later.');
                 Mage::app()->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
                 
                 Mage::logException($e);
                 //$this->_goBack();
             } 

             $this->_clearsession();
        

    }
    




    }

?>
