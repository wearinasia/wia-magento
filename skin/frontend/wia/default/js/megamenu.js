jQuery(document).ready(function($) {

    "use strict";



    $("ul.dropdown > li").hover(
        function (e) {
            if ($(window).width() > 943) {
                $(this).children("ul").show();
                e.preventDefault();
            }
        }, 
        function (e) {
            if ($(window).width() > 943) {
                $(this).children("ul").hide();
                e.preventDefault();
            }
        }
    );
    
    $("ul.dropdown-megamenu > li").hover(
        function (e) {
            if ($(window).width() > 943) {
                $(this).children("ul").show();
                e.preventDefault();
            }
        }, 
        function (e) {
            if ($(window).width() > 943) {
                $(this).children("ul").hide();
                e.preventDefault();
            }
        }
    );
   

});
